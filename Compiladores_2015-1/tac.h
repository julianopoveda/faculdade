#ifndef TAC_HEADER
#define TAC_HEADER

#include <stdio.h>
#include "hash.h"
#include "astree.h"

#define EXPR_UNDEFINED	0
#define EXPR_PARAM		1
#define EXPR_OUTPUT		2

#define TAC_UNDEFINED   	    0
#define TAC_SYMBOL  		    1
#define TAC_OP_SUM              2 //TAC_ADD			2
#define TAC_OP_SUB		        3 //TAC_SUB
#define TAC_OP_MUL	    	    4 //TAC_MUL
#define TAC_OP_DIV  		    5 //TAC_DIV
#define TAC_OP_GRE		        6 //TAC_GT
#define TAC_OP_LES	    	    7 //TAC_LT
#define TAC_OP_LE		        8 //TAC_LE
#define TAC_OP_GE	        	9 //TAC_GE
#define TAC_OP_EQ   	    	10 //TAC_EQ
#define TAC_OP_NE	    	    11 //TAC_NE
#define TAC_OP_AND	        	12 //TAC_AND
#define TAC_OP_OR	        	13 //TAC_OR
#define TAC_ATR_VAR 	    	14 //TAC_ASS
#define TAC_RET		            15 //TAC_RETURN
#define TAC_LOOP		        16 //TAC_JUMP
#define TAC_LABEL	    	    17 //TAC_LABEL
#define TAC_IFZERO  		    18 //TAC_IFZ
#define TAC_CHAM_F		        19 //TAC_CALL
#define TAC_DECL_VEC	        20 //TAC_ARR
#define TAC_ATR_VEC		        21 //TAC_ARR_ASS
#define TAC_POINTER_VALUE		22 //TAC_DREF
#define TAC_POINTER_ADDRESS		23//TAC_REF
#define TAC_OUT		            24//TAC_OUTPUT
#define TAC_INP		            25//TAC_INPUT
#define TAC_LIST_E		        26//TAC_EXPRLST
#define TAC_BEGIN_FUNC	        27//TAC_BEGINFUN
#define TAC_END_FUNC		    28//TAC_ENDFUN
#define TAC_PARAM		        29//TAC_PARAM

//adicionado---------------------------
#define SYMBOL_UNDEFINED	0
#define SYMBOL_LIT_INTEGER	1
#define SYMBOL_LIT_FLOATING	2
#define SYMBOL_LIT_TRUE		3
#define SYMBOL_LIT_FALSE	4
#define SYMBOL_LIT_CHAR		5
#define SYMBOL_LIT_STRING	6
#define SYMBOL_IDENTIFIER	7
#define SYMBOL_LABEL		8

#define DECL_UNDEFINED	0
#define DECL_VAR		1
#define DECL_FUNC		2
#define DECL_ARRAY		3
#define DECL_PT			4
//-------------------------------------

typedef struct tac_node {
	int type;
	HASH_NODE *res;
	HASH_NODE *op1;
	HASH_NODE *op2;
	struct tac_node *prev;
	struct tac_node *next;
} TAC_NODE;

TAC_NODE *tacCreate(int type, HASH_NODE *res, HASH_NODE *op1, HASH_NODE *op2);
void tacPrintSingle(TAC_NODE *node);
void tacPrint(TAC_NODE *first);
TAC_NODE *tacJoin(TAC_NODE *l1, TAC_NODE *l2);
TAC_NODE *tacInvert(TAC_NODE *list);
TAC_NODE *makeTac(ASTREE *node, int exprType);

TAC_NODE *tacBinOp(int type, TAC_NODE *op1, TAC_NODE *op2);
TAC_NODE *tacReturn(TAC_NODE *node);
TAC_NODE *tacLoop(TAC_NODE *expr,TAC_NODE *cmd);
TAC_NODE *tacCall(TAC_NODE *node, HASH_NODE *symbol);
TAC_NODE *tacParam(TAC_NODE *node);
TAC_NODE *tacIf(TAC_NODE *expr, TAC_NODE *cmd);
TAC_NODE *tacIfElse(TAC_NODE *expr, TAC_NODE *cmdThen, TAC_NODE *cmdElse);
TAC_NODE *tacArr(HASH_NODE* symbol, TAC_NODE *node);
TAC_NODE *tacArrAss(HASH_NODE* symbol, TAC_NODE *index, TAC_NODE *expr);
TAC_NODE *tacAss(HASH_NODE *symbol, TAC_NODE *expr);
TAC_NODE *tacFunc(HASH_NODE* symbol, TAC_NODE *node);
TAC_NODE *tacInput(HASH_NODE *symbol);
TAC_NODE *tacOutput(TAC_NODE *node);

void tacGetOperators(TAC_NODE *node, FILE *file);
void tacToAssembly(TAC_NODE *list, FILE *file);
void writeAssembly(TAC_NODE *list, FILE *file);


#endif