#ifndef TAC_HEADER
#define TAC_HEADER

#include <stdio.h>
#include "hash.h"
#include "astree.h"

#define EXPR_UNDEFINED	0
#define EXPR_PARAM		1
#define EXPR_OUTPUT		2

#define TAC_UNDEFINED   	    0
#define TAC_SYMBOL  		    1
#define TAC_OP_SUM              2 
#define TAC_OP_SUB		        3 
#define TAC_OP_MUL	    	    4 
#define TAC_OP_DIV  		    5 
#define TAC_OP_GRE		        6 
#define TAC_OP_LES	    	    7 
#define TAC_OP_LE		        8 
#define TAC_OP_GE	        	9 
#define TAC_OP_EQ   	    	10 
#define TAC_OP_NE	    	    11 
#define TAC_OP_AND	        	12 
#define TAC_OP_OR	        	13 
#define TAC_ATR_VAR 	    	14 
#define TAC_RET		            15 
#define TAC_LOOP		        16 
#define TAC_LABEL	    	    17 
#define TAC_IFZERO  		    18 
#define TAC_CHAM_F		        19 
#define TAC_DECL_VEC	        20 
#define TAC_ATR_VEC		        21 
#define TAC_POINTER_VALUE		22 
#define TAC_POINTER_ADDRESS		23
#define TAC_OUT		            24
#define TAC_INP		            25
#define TAC_LIST_E		        26
#define TAC_BEGIN_FUNC	        27
#define TAC_END_FUNC		    28
#define TAC_PARAM		        29

typedef struct tac_node {
	int type;
	HASH_NODE *res;
	HASH_NODE *op1;
	HASH_NODE *op2;
	struct tac_node *prev;
	struct tac_node *next;
} TAC_NODE;

TAC_NODE *tacCreate(int type, HASH_NODE *res, HASH_NODE *op1, HASH_NODE *op2);
void tacPrintSingle(TAC_NODE *node);
void tacPrint(TAC_NODE *first);
TAC_NODE *tacJoin(TAC_NODE *l1, TAC_NODE *l2);
TAC_NODE *tacInvert(TAC_NODE *list);
TAC_NODE *makeTac(ASTREE *node, int exprType);

TAC_NODE *tacBinOp(int type, TAC_NODE *op1, TAC_NODE *op2);
TAC_NODE *tacReturn(TAC_NODE *node);
TAC_NODE *tacLoop(TAC_NODE *expr,TAC_NODE *cmd);
TAC_NODE *tacCall(TAC_NODE *node, HASH_NODE *symbol);
TAC_NODE *tacParam(TAC_NODE *node);
TAC_NODE *tacIf(TAC_NODE *expr, TAC_NODE *cmd);
TAC_NODE *tacIfElse(TAC_NODE *expr, TAC_NODE *cmdThen, TAC_NODE *cmdElse);
TAC_NODE *tacArr(HASH_NODE* symbol, TAC_NODE *node);
TAC_NODE *tacArrAss(HASH_NODE* symbol, TAC_NODE *index, TAC_NODE *expr);
TAC_NODE *tacAss(HASH_NODE *symbol, TAC_NODE *expr);
TAC_NODE *tacFunc(HASH_NODE* symbol, TAC_NODE *node);
TAC_NODE *tacInput(HASH_NODE *symbol);
TAC_NODE *tacOutput(TAC_NODE *node);

#endif
