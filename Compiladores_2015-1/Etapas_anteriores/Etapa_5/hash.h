#ifndef HASH_HEADER
#define HASH_HEADER
/*Juliano Poveda 213995
Luiz Fernando Ramos Cardozo 207113*/

#define HASH_SIZE 997
#define MAX_BUFFER 256

#define SYMBOL_UNDEFINED	0
#define SYMBOL_LIT_INTEGER	1
#define SYMBOL_LIT_FLOATING	2
#define SYMBOL_LIT_TRUE		3
#define SYMBOL_LIT_FALSE	4
#define SYMBOL_LIT_CHAR		5
#define SYMBOL_LIT_STRING	6
#define SYMBOL_IDENTIFIER	7
#define SYMBOL_LABEL		8

//defines de datatype
//-------
#define DECL_UNDEFINED 0
#define DECL_VAR		1
#define DECL_FUNC		2
#define DECL_VEC		3
#define DECL_PT			4


//-------
#define DATATYPE_UNDEFINED 0
#define DATATYPE_WORD 1
#define DATATYPE_BOOL 2
#define DATATYPE_BYTE 3
#define DATATYPE_POINTER 4

typedef struct hash_node
{
	char *text;
	int type;
	int dataType;
	int declType;
	int declCount;
	struct hash_node *paramLst;
	struct hash_node* next;
}HASH_NODE;

HASH_NODE *Table[HASH_SIZE];
char tempBuffer[MAX_BUFFER];
char labelBuffer[MAX_BUFFER];
int nextTemp;
int nextLabel;
int SemanticErrors;

void hashInit(void);
void hashPrint(void);
int hashAddress(char* text);
HASH_NODE* hashInsert(char* text, int type);
HASH_NODE* hashFind(char* text);
void initMe(void);
HASH_NODE *makeTemp(void);
HASH_NODE *makeLabel(void);

#endif
