#include "semantic.h"

void semanticSetTypes(ASTREE *node) {
	int i=0;
	ASTREE *tempNode;
	HASH_NODE *tempLst;

	if (!node) return;

	if (node->type == AST_DECL_VAR ||
		node->type == AST_DECL_VEC ||
		node->type == AST_DECL_PONT ||
		node->type == AST_DEF_F ||
		node->type == AST_PARAM) {
		if (node->symbol->declCount++ > 1) {
			fprintf(stderr,"Line %d: identifier %s redeclaration.\n", node->lineNumber, node->symbol->text);
			SemanticErrors++;
		}

		switch (node->type) {
			case AST_DECL_VAR:
				node->symbol->declType = DECL_VAR;
				break;
			case AST_DECL_VEC:
				node->symbol->declType = DECL_VEC;
				break;
			case AST_DECL_PONT:
				node->symbol->declType = DECL_PT;
				break;
			case AST_DEF_F:
				node->symbol->declType = DECL_FUNC;
				break;
			case AST_LIST_P:
				node->symbol->declType = DECL_UNDEFINED;
				tempLst = node->symbol;
				tempNode = node;
				if (tempNode->son[1]) {
					tempNode = tempNode->son[1];
					while (tempNode->son[1]) {
						tempLst->paramLst = tempNode->symbol;
						tempLst = tempNode->symbol;
						tempNode = tempNode->son[1];
					}
					if(tempNode->symbol)
						tempLst->paramLst = tempNode->symbol;
				}
				break;
			case AST_PARAM:
				node->symbol->declType = DECL_VAR;
				break;
			default:
				node->symbol->declType = DECL_UNDEFINED;
				break;
		}

		if (node->son[0]) {
			switch (node->son[0]->type) {
				case AST_T_WOR:
					node->symbol->dataType = DATATYPE_WORD;
					break;
				case AST_T_BOO:
					node->symbol->dataType = DATATYPE_BOOL;
					break;
				case AST_T_BYT:
					node->symbol->dataType = DATATYPE_BYTE;
					break;
				default:
					node->symbol->dataType = DATATYPE_UNDEFINED;
					break;
			}
		}
	}

	for (i = 0; i < MAX_SONS; ++i) {
		if (node->son[i]) semanticSetTypes(node->son[i]);
	}
}

void semanticCheckDeclarations(ASTREE *node){
	int i;

	if (!node) return;

	if (node->type == AST_SYMBOL ||
		node->type == AST_SYMBOL_VEC ||
		node->type == AST_POINTER_ADDRESS ||
		node->type == AST_POINTER_VALUE ||
		node->type == AST_CHAM_F ||
		node->type == AST_ATR_VAR ||
		node->type == AST_ATR_VEC ||
		node->type == AST_INP) {
		if (node->symbol->declType == DECL_UNDEFINED) {
			fprintf(stderr,"Line %d: identifier %s undefined.\n", node->lineNumber, node->symbol->text);
			SemanticErrors++;
		}
	}

	for (i = 0; i < MAX_SONS; ++i) {
		if (node->son[i]) semanticCheckDeclarations(node->son[i]);
	}
}

void usageCheck(ASTREE *node) {
	int i;

	if (!node) return;

	if (node->symbol) {
		switch (node->type) {
			case AST_SYMBOL:
			case AST_ATR_VAR:
				if (node->symbol->declType != DECL_VAR && node->symbol->declType != DECL_PT) {
					fprintf(stderr,"Line %d: using non-variable/pointer symbol %s as variable/pointer.\n", node->lineNumber, node->symbol->text);
					SemanticErrors++;
				}
				break;
			case AST_SYMBOL_VEC:
			case AST_ATR_VEC:
				if (node->symbol->declType != DECL_VEC) {
					fprintf(stderr,"Line %d: using non-array symbol %s as array.\n", node->lineNumber, node->symbol->text);
					SemanticErrors++;
				}
				break;
			case AST_CHAM_F:
				if (node->symbol->declType != DECL_FUNC) {
					fprintf(stderr,"Line %d: using non-function symbol %s as function.\n", node->lineNumber, node->symbol->text);
					SemanticErrors++;
				}
				break;
			case AST_POINTER_ADDRESS:
				if (node->symbol->declType != DECL_VAR) {
					fprintf(stderr,"Line %d: using non-variable symbol %s as variable.\n", node->lineNumber, node->symbol->text);
					SemanticErrors++;
				}
				break;
			case AST_POINTER_VALUE:
				if (node->symbol->declType != DECL_PT) {
					fprintf(stderr,"Line %d: using non-pointer symbol %s as pointer.\n", node->lineNumber, node->symbol->text);
					SemanticErrors++;
				}
				break;
			default:
				break;
		}
	}

	for (i = 0; i < MAX_SONS; ++i) {
		if (node->son[i]) usageCheck(node->son[i]);
	}
}

int outputCheck(ASTREE *node) {
	int ok = 1;

	if (!node) return 0;

	switch (node->type) {
		case AST_SYMBOL_LIT_INT:
		case AST_SYMBOL_LIT_FALSE:
		case AST_SYMBOL_LIT_TRUE:
		case AST_SYMBOL_LIT_CHAR:
		case AST_SYMBOL_LIT_STRING:
		case AST_OP_SUM:
		case AST_OP_SUB:
		case AST_OP_MUL:
		case AST_OP_DIV:
		case AST_SYMBOL:
		case AST_SYMBOL_VEC:
		case AST_OP_LE:
		case AST_OP_GE:
		case AST_OP_EQ:
		case AST_OP_NE:
		case AST_OP_AND:
		case AST_OP_OR:
		case AST_OP_LES:
		case AST_OP_GRE:
		case AST_POINTER_ADDRESS:
		case AST_POINTER_VALUE:
		case AST_CHAM_F:
		case AST_PARENTESIS:
			break;
		case AST_LIST_E:
			ok = outputCheck(node->son[0]) * ok;
			ok = outputCheck(node->son[1]) * ok;
			break;
		default:
			ok = 0;
			break;
	}

	return ok;
}

int returnCheck(ASTREE *node, int returnType){
	if(!node) return 0;
	int i;
	int tempData;
	int tempReturn = 0;

	if (node->type == AST_RET) {	
		if (node->son[0] != NULL) {
			tempData = typeCheck(node);
			if (tempData != returnType &&
			  !(tempData == DATATYPE_WORD && returnType == DATATYPE_BYTE) &&
		   	  !(tempData == DATATYPE_BYTE && returnType == DATATYPE_WORD)) {
				fprintf(stderr, "Line %d: Error on return definition\n", node->lineNumber);
				SemanticErrors++;
			}
			else
				tempReturn++;
		}
  	}
	for (i = 0; i < MAX_SONS; ++i) {
		tempReturn += returnCheck(node->son[i],returnType);
	}
	return tempReturn;
}

int typeCheck(ASTREE *node) {
	int i = 0;
	int tempData;
	int tempSize;
	int retorno = 0;
	
	ASTREE *tempNode;
	HASH_NODE *tempLst;

	if (!node) return DATATYPE_UNDEFINED;

	switch (node->type) {
		case AST_PROG:
			typeCheck(node->son[0]);
			typeCheck(node->son[1]);
			return DATATYPE_UNDEFINED;
			break;
		case AST_T_WOR:
			return DATATYPE_WORD;
			break;
		case AST_T_BOO:
			return DATATYPE_BOOL;
			break;
		case AST_T_BYT:
			return DATATYPE_BYTE;
			break;
		case AST_SYMBOL_LIT_INT:
			return DATATYPE_WORD;
			break;
		case AST_SYMBOL_LIT_FALSE:
			return DATATYPE_BOOL;
			break;
		case AST_SYMBOL_LIT_TRUE:
			return DATATYPE_BOOL;
			break;
		case AST_SYMBOL_LIT_CHAR:
			return DATATYPE_BYTE;
			break;
		case AST_SYMBOL_LIT_STRING:
			// Não definido pois String só pode ser passada por output
			return DATATYPE_UNDEFINED;
			break;
		case AST_DECL_VAR:
			if (node->symbol->dataType == typeCheck(node->son[1]) ||
				node->symbol->dataType == DATATYPE_BYTE && typeCheck(node->son[1]) == DATATYPE_WORD ||
				node->symbol->dataType == DATATYPE_WORD && typeCheck(node->son[1]) == DATATYPE_BYTE) {
				return DATATYPE_UNDEFINED;
			} else {
				fprintf(stderr,"Line %d: type missmatch at symbol %s declaration.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_DECL_VEC:
			tempSize = atoi(node->son[1]->symbol->text);
			if (tempSize <= 0) {
				fprintf(stderr,"Line %d: array %s bad size.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
				return DATATYPE_UNDEFINED;
			}
			if (node->son[2]) {
				tempNode = node->son[2];
				if (tempNode->son[1]) {
					while (i < tempSize) {
						if (node->symbol->dataType == typeCheck(tempNode->son[1]) ||
							node->symbol->dataType == DATATYPE_BYTE && typeCheck(node->son[1]) == DATATYPE_WORD ||
							node->symbol->dataType == DATATYPE_WORD && typeCheck(node->son[1]) == DATATYPE_BYTE) {
							i++;
							if (tempNode->son[0]->symbol) {
								i++;
								if ((node->symbol->dataType == typeCheck(tempNode->son[0]) ||
									node->symbol->dataType == DATATYPE_BYTE && typeCheck(node->son[0]) == DATATYPE_WORD ||
									node->symbol->dataType == DATATYPE_WORD && typeCheck(node->son[0]) == DATATYPE_BYTE) && 
									 i == tempSize) {
									return DATATYPE_UNDEFINED;
								} else {
									fprintf(stderr,"Line %d: size or type missmatch at symbol %s declaration.\n", node->lineNumber, node->symbol->text);
									SemanticErrors++;
									return DATATYPE_UNDEFINED;
								}
							} else {
								tempNode = tempNode->son[0];
							}
						} else {
							fprintf(stderr,"Line %d: type missmatch at symbol %s declaration.\n", node->lineNumber, node->symbol->text);
							SemanticErrors++;
							return DATATYPE_UNDEFINED;
						}
					}
				} else {
					i++;
					if ((node->symbol->dataType == typeCheck(tempNode) ||
				    	 node->symbol->dataType == DATATYPE_BYTE && typeCheck(tempNode) == DATATYPE_WORD ||
				    	 node->symbol->dataType == DATATYPE_WORD && typeCheck(tempNode) == DATATYPE_BYTE) && 
						 i == tempSize) {
						return DATATYPE_UNDEFINED;
					} else {
						fprintf(stderr,"Line %d: size or type missmatch at symbol %s declaration.\n", node->lineNumber, node->symbol->text);
						SemanticErrors++;
						return DATATYPE_UNDEFINED;
					}
				}
			}
			break;
		case AST_INI_VEC:
			return DATATYPE_UNDEFINED;
			break;
		case AST_DECL_PONT:
			if (typeCheck(node->son[1]) == DATATYPE_WORD) {
				return DATATYPE_UNDEFINED;
			} else {
				fprintf(stderr,"Line %d: type missmatch at symbol %s declaration.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_DEF_F:
			retorno = returnCheck(node->son[3], node->symbol->dataType);//2
			typeCheck(node->son[3]);
			return retorno;
			break;
		case AST_LIST_P:
			retorno = returnCheck(node->son[2], node->symbol->dataType);//2
			typeCheck(node->son[2]);
			return retorno;
			break;
		case AST_PARAM:
			return DATATYPE_UNDEFINED;
			break;
		case AST_IF:
			if (typeCheck(node->son[0]) == DATATYPE_BOOL) {
				return DATATYPE_UNDEFINED;
			} else {
				fprintf(stderr,"Line %d: IF expects boolean.\n", node->lineNumber);
				SemanticErrors++;
			}
			typeCheck(node->son[1]);
			return DATATYPE_UNDEFINED;
			break;
		case AST_IF_ELSE:
			if (typeCheck(node->son[0]) == DATATYPE_BOOL) {
				return DATATYPE_UNDEFINED;
			} else {
				fprintf(stderr,"Line %d: IF_ELSE expects boolean.\n", node->lineNumber);
				SemanticErrors++;
			}
			typeCheck(node->son[1]);
			typeCheck(node->son[2]);
			return DATATYPE_UNDEFINED;
			break;
		case AST_LOOP:
			typeCheck(node->son[0]);
			if (typeCheck(node->son[1]) == DATATYPE_BOOL) {
				return DATATYPE_UNDEFINED;
			} else {
				fprintf(stderr,"Line %d: LOOP expects boolean.\n", node->lineNumber);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_OP_SUM:
			tempData = typeCheck(node->son[0]);
			switch (tempData) {
				case DATATYPE_WORD:
				case DATATYPE_BYTE:
					tempData = typeCheck(node->son[1]);
					if (tempData == DATATYPE_BYTE) {
						return DATATYPE_BYTE;
					} else if (tempData == DATATYPE_WORD) {
						return DATATYPE_WORD;
					} else if (tempData == DATATYPE_POINTER) {
						return DATATYPE_POINTER;
					}
					break;
				case DATATYPE_POINTER:
					tempData = typeCheck(node->son[1]);
					if (tempData == DATATYPE_WORD) {
						return DATATYPE_POINTER;
					}
					break;
				default:
					break;
			}
			fprintf(stderr,"Line %d: operands type missmatch.\n", node->lineNumber);
			SemanticErrors++;
			return DATATYPE_UNDEFINED;
			break;
		case AST_OP_SUB:
		case AST_OP_MUL:
		case AST_OP_DIV:
			tempData = typeCheck(node->son[0]);
			switch (tempData) {
				case DATATYPE_WORD:
				case DATATYPE_BYTE:
					tempData = typeCheck(node->son[1]);
					if (tempData == DATATYPE_BYTE) {
						return DATATYPE_BYTE;
					} else if (tempData == DATATYPE_WORD) {
						return DATATYPE_WORD;
					}
					break;
				default:
					break;
			}
			fprintf(stderr,"Line %d: operands type missmatch.\n", node->lineNumber);
			SemanticErrors++;
			return DATATYPE_UNDEFINED;
			break;
		case AST_PARENTESIS:
			return typeCheck(node->son[0]);
			break;
		case AST_SYMBOL:
			if (node->symbol->declType == DATATYPE_POINTER) {
				return DATATYPE_POINTER;
			} else {
				return node->symbol->dataType;
			}
			break;
		case AST_SYMBOL_VEC:
			tempData = typeCheck(node->son[0]);
			if (tempData == DATATYPE_WORD) {
				return node->symbol->dataType;
			} else {
				fprintf(stderr,"Line %d: type missmatch at symbol %s index.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_OP_LE:
		case AST_OP_GE:
		case AST_OP_GRE:
		case AST_OP_LES:
			tempData = typeCheck(node->son[0]);
			switch (tempData) {
				case DATATYPE_WORD:
					tempData = typeCheck(node->son[1]);
					if (tempData == DATATYPE_WORD ||
						tempData == DATATYPE_BYTE) {
						return DATATYPE_BOOL;
					}
					break;
				case DATATYPE_BYTE:
					tempData = typeCheck(node->son[1]);
					if (tempData == DATATYPE_BYTE || 
						tempData == DATATYPE_WORD) {
						return DATATYPE_BOOL;
					}
					break;
				default:
					break;
			}
			fprintf(stderr,"Line %d: operands type missmatch.\n", node->lineNumber);
			SemanticErrors++;
			return DATATYPE_UNDEFINED;
			break;
		case AST_OP_EQ:
		case AST_OP_NE:
			tempData = typeCheck(node->son[0]);
			switch (tempData) {
				case DATATYPE_WORD:
					tempData = typeCheck(node->son[1]);
					if (tempData == DATATYPE_WORD ||
						tempData == DATATYPE_BYTE) {
						return DATATYPE_BOOL;
					}
					break;
				case DATATYPE_BYTE:
					tempData = typeCheck(node->son[1]);
					if (tempData == DATATYPE_BYTE || 
						tempData == DATATYPE_WORD) {
						return DATATYPE_BOOL;
					}
					break;
				case DATATYPE_BOOL:
					tempData = typeCheck(node->son[1]);
					if (tempData == DATATYPE_BOOL) {
						return DATATYPE_BOOL;
					}
					break;
				default:
					break;
			}
			fprintf(stderr,"Line %d: operands type missmatch.\n", node->lineNumber);
			SemanticErrors++;
			return DATATYPE_UNDEFINED;
			break;
		case AST_OP_AND:
		case AST_OP_OR:
			tempData = typeCheck(node->son[0]);
			if ((tempData == DATATYPE_BOOL) && (tempData == typeCheck(node->son[1]))) {
				return DATATYPE_BOOL;
			} else {
				fprintf(stderr,"Line %d: operands type missmatch.\n", node->lineNumber);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_POINTER_ADDRESS:
			if (node->symbol->declType != DECL_PT) {
				return DATATYPE_POINTER;
			} else {
				fprintf(stderr,"Line %d: using non-referenceable symbol %s as reference.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_POINTER_VALUE:
			if (node->symbol->declType == DECL_PT) {
				return node->symbol->dataType;
			} else {
				fprintf(stderr,"Line %d: using non-pointer symbol %s as pointer.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_CHAM_F:
			tempNode = node;
			if (tempNode->son[0]) {
				if (tempNode->son[0]->type != AST_LIST_E) {
					if (tempNode->symbol->paramLst && !tempNode->symbol->paramLst->paramLst) {
						tempData = typeCheck(tempNode->son[0]);
						switch (tempNode->symbol->paramLst->dataType) {
							case DATATYPE_WORD:
							case DATATYPE_BYTE:
								if (tempData == DATATYPE_WORD || tempData == DATATYPE_BYTE) {
									return node->symbol->dataType;
								} else {
									fprintf(stderr,"Line %d: missmatch parameters at %s function call.\n", node->lineNumber, node->symbol->text);
									SemanticErrors++;
									return DATATYPE_UNDEFINED;
								}
								break;
							case DATATYPE_BOOL:
								if (tempData == DATATYPE_BOOL) {
									return node->symbol->dataType;
								} else {
									fprintf(stderr,"Line %d: missmatch parameters at %s function call.\n", node->lineNumber, node->symbol->text);
									SemanticErrors++;
									return DATATYPE_UNDEFINED;
								}
								break;
							default:
								fprintf(stderr,"CALL ERROR.\n");
								return DATATYPE_UNDEFINED;
								break;
						}
					} else {
						fprintf(stderr,"Line %d: wrong parameters number at %s function call.\n", node->lineNumber, node->symbol->text);
						SemanticErrors++;
						return DATATYPE_UNDEFINED;
					}
				} else {
					tempLst = tempNode->symbol->paramLst;
					tempNode = node->son[0];
					while (tempNode->son[0]->type == AST_LIST_E) {
						if (tempLst && tempLst->paramLst && tempLst->paramLst->paramLst) {
							tempData = typeCheck(tempNode->son[1]);
							switch (tempLst->dataType) {
								case DATATYPE_WORD:
								case DATATYPE_BYTE:
									if (tempData == DATATYPE_WORD || tempData == DATATYPE_BYTE) {
										return node->symbol->dataType;
									} else {
										fprintf(stderr,"Line %d: missmatch parameters at %s function call.\n", node->lineNumber, node->symbol->text);
										SemanticErrors++;
										return DATATYPE_UNDEFINED;
									}
									break;
								case DATATYPE_BOOL:
									if (tempData == DATATYPE_BOOL) {
										return node->symbol->dataType;
									} else {
										fprintf(stderr,"Line %d: missmatch parameters at %s function call.\n", node->lineNumber, node->symbol->text);
										SemanticErrors++;
										return DATATYPE_UNDEFINED;
									}
									break;
								default:
									fprintf(stderr,"CALL ERROR.\n");
									return DATATYPE_UNDEFINED;
									break;
							}
							tempLst = tempLst->paramLst;
							tempNode = tempNode->son[0];
						} else {
							fprintf(stderr,"Line %d: wrong parameters number at %s function call.\n", node->lineNumber, node->symbol->text);
							SemanticErrors++;
							return DATATYPE_UNDEFINED;
						}
					}

					if (tempLst && tempLst->paramLst && !tempLst->paramLst->paramLst) {
						for (i = 1; i > -1; --i) {
							tempData = typeCheck(tempNode->son[i]);
							switch (tempLst->dataType) {
								case DATATYPE_WORD:
								case DATATYPE_BYTE:
									if (tempData == DATATYPE_WORD || tempData == DATATYPE_BYTE) {
										return node->symbol->dataType;
									} else {
										fprintf(stderr,"Line %d: missmatch parameters at %s function call.\n", node->lineNumber, node->symbol->text);
										SemanticErrors++;
										return DATATYPE_UNDEFINED;
									}
									break;
								case DATATYPE_BOOL:
									if (tempData == DATATYPE_BOOL) {
										return node->symbol->dataType;
									} else {
										fprintf(stderr,"Line %d: missmatch parameters at %s function call.\n", node->lineNumber, node->symbol->text);
										SemanticErrors++;
										return DATATYPE_UNDEFINED;
									}
									break;
								default:
									fprintf(stderr,"CALL ERROR.\n");
									return DATATYPE_UNDEFINED;
									break;
							}
							tempLst = tempLst->paramLst;
						}
					} else {
						fprintf(stderr,"Line %d: wrong parameters number at %s function call.\n", node->lineNumber, node->symbol->text);
						SemanticErrors++;
						return DATATYPE_UNDEFINED;
					}
				}
			} else if (tempNode->symbol->paramLst) {
				fprintf(stderr,"Line %d: missing parameters at %s function call.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
				return DATATYPE_UNDEFINED;
			} else {
				return tempNode->symbol->dataType;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_LIST_E:
			typeCheck(node->son[0]);
			typeCheck(node->son[1]);
			return DATATYPE_UNDEFINED;
			break;
		case AST_COM://Não tem tratamento ainda
			return typeCheck(node->son[0]);
			break;
		case AST_BLO_COM:
			return typeCheck(node->son[0]);
			break;
		case AST_SEQ:
			typeCheck(node->son[0]);
			if (node->son[1]) {
				typeCheck(node->son[1]);
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_ATR_VAR:
			tempData = typeCheck(node->son[0]);
			if (node->symbol->declType == DECL_PT && tempData == DATATYPE_POINTER) {
				return DATATYPE_UNDEFINED;
			} else if (node->symbol->dataType == tempData ||
					   node->symbol->dataType == DATATYPE_BYTE && tempData == DATATYPE_WORD ||
					   node->symbol->dataType == DATATYPE_WORD && tempData == DATATYPE_BYTE) {
				return DATATYPE_UNDEFINED;
			} else {
				fprintf(stderr,"Line %d: type missmatch at symbol %s assignment.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_ATR_VEC:
			tempData = typeCheck(node->son[0]);
			if (tempData == DATATYPE_WORD) {
				if (node->symbol->dataType == typeCheck(node->son[1]) ||
					node->symbol->dataType == DATATYPE_WORD && typeCheck(node->son[1]) == DATATYPE_BYTE) {
					return DATATYPE_UNDEFINED;
				} else {
					fprintf(stderr,"Line %d: type missmatch at symbol %s assignment.\n", node->lineNumber, node->symbol->text);
					SemanticErrors++;
				}
			} else {
				fprintf(stderr,"Line %d: type missmatch at symbol %s index.\n", node->lineNumber, node->symbol->text);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_INP:
			return node->symbol->dataType;
			break;
		case AST_OUT:
			if (outputCheck(node->son[0])) {
				typeCheck(node->son[0]);
			} else {
				fprintf(stderr,"Line %d: OUTPUT expects string or arithmetic operation.\n", node->lineNumber);
				SemanticErrors++;
			}
			return DATATYPE_UNDEFINED;
			break;
		case AST_RET:
			return typeCheck(node->son[0]);
			break;	
		default:
			return DATATYPE_UNDEFINED;
			break;
	}
}
