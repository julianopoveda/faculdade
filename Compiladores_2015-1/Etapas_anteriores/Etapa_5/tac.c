#include <stdio.h>
#include <stdlib.h>
#include "tac.h"

TAC_NODE *tacCreate(int type, HASH_NODE *res, HASH_NODE *op1, HASH_NODE *op2) {
	TAC_NODE *node;
	node = (TAC_NODE*)calloc(1, sizeof(TAC_NODE));
	node->type = type;
	node->res = res;
	node->op1 = op1;
	node->op2 = op2;
	node->prev = 0;
	node->next = 0;
	
	return node;
}

void tacPrintSingle(TAC_NODE *node) {
	
	if(!node) return;

	if (node->type != TAC_SYMBOL) {
		fprintf(stderr, "(");
	}
	
	//printf("tipo %d\n", node->type);
	switch(node->type) {
		case TAC_OP_SUM:			fprintf(stderr, "TAC_OP_SUM"); break;
		case TAC_OP_SUB:			fprintf(stderr, "TAC_OP_SUB"); break;
		case TAC_OP_MUL:			fprintf(stderr, "TAC_OP_MUL"); break;
		case TAC_OP_DIV:			fprintf(stderr, "TAC_OP_DIV"); break;
		case TAC_OP_GRE:			fprintf(stderr, "TAC_OP_GRE"); break;
		case TAC_OP_LES:			fprintf(stderr, "TAC_OP_LES"); break;
		case TAC_OP_LE:				fprintf(stderr, "TAC_OP_LE"); break;
		case TAC_OP_GE:				fprintf(stderr, "TAC_OP_GE"); break;
		case TAC_OP_EQ:				fprintf(stderr, "TAC_OP_EQ"); break;
		case TAC_OP_NE:				fprintf(stderr, "TAC_OP_NE"); break;
		case TAC_OP_OR:				fprintf(stderr, "TAC_OP_OR"); break;
		case TAC_OP_AND:			fprintf(stderr, "TAC_OP_AND"); break;
		case TAC_RET:				fprintf(stderr, "TAC_RET"); break;
		case TAC_SYMBOL:			break;
		case TAC_LOOP:				fprintf(stderr, "TAC_LOOP"); break;
		case TAC_IFZERO:			fprintf(stderr, "TAC_IFZERO"); break;
		case TAC_LABEL:				fprintf(stderr, "TAC_LABEL"); break;
		case TAC_CHAM_F:			fprintf(stderr, "TAC_CHAM_F"); break;
		case TAC_DECL_VEC:			fprintf(stderr, "TAC_DECL_VEC"); break;
		case TAC_ATR_VEC:			fprintf(stderr, "TAC_ATR_VEC"); break;
		case TAC_ATR_VAR:			fprintf(stderr, "TAC_ATR_VAR"); break;
		case TAC_POINTER_VALUE:		fprintf(stderr, "TAC_POINTER_VALUE"); break;
		case TAC_POINTER_ADDRESS:	fprintf(stderr, "TAC_POINTER_ADDRESS"); break;
		case TAC_OUT:				fprintf(stderr, "TAC_OUT"); break;
		case TAC_INP:				fprintf(stderr, "TAC_INP"); break;
		case TAC_LIST_E:			fprintf(stderr, "TAC_LIST_E"); break;
		case TAC_BEGIN_FUNC:		fprintf(stderr, "TAC_BEGIN_FUNC"); break;
		case TAC_END_FUNC:			fprintf(stderr, "TAC_END_FUNC"); break;
		case TAC_PARAM:				fprintf(stderr, "TAC_PARAM"); break;
		default:					fprintf(stderr, "UNDEFINED %d", node->type); break;
	}
	
	if (node->type != TAC_SYMBOL) {
		if (node->res) fprintf(stderr, ", %s", node->res->text); else fprintf(stderr, ", -");
		if (node->op1) fprintf(stderr, ", %s", node->op1->text); else fprintf(stderr, ", -");
		if (node->op2) fprintf(stderr, ", %s", node->op2->text); else fprintf(stderr, ", -");
		fprintf(stderr, ");\n");
	}
}

void tacPrint(TAC_NODE *first) {
	TAC_NODE *node;

	for (node = first; node; node = node->next) {
		tacPrintSingle(node);
	}
}

TAC_NODE *tacJoin(TAC_NODE *l1, TAC_NODE *l2) {
	TAC_NODE *node;

	if (!l1) return l2;

	if (!l2) return l1;
	
	for (node = l2; node->prev; node = node->prev);
	node->prev = l1;
	
	return l2;
}

TAC_NODE *tacInvert(TAC_NODE *list) {
	TAC_NODE *node = list;
	
	if (node) {
		for (node = list; node->prev; node = node->prev) {
			node->prev->next = node;
		}
	}
	return node;
}

TAC_NODE *makeTac(ASTREE *node, int exprType) {
	int i;
	int tempExpr = exprType;
	TAC_NODE *tempTac;
	TAC_NODE *code[MAX_SONS];

	if (!node) return 0;

	switch (node->type) {
		case AST_CHAM_F:
			tempExpr = EXPR_PARAM;
			break;
		case AST_OUT:
			tempExpr = EXPR_OUTPUT;
			break;
		default:
			break;
	}

	for (i = 0; i < MAX_SONS; ++i) {
		code[i] = makeTac(node->son[i], tempExpr);
	}
	//printf("tipo %d\n", node->type);
	switch (node->type) {
		case AST_PROG: 
			tempTac = tacJoin(code[0],code[1]);
			break;
		case AST_SYMBOL_LIT_INT:
		case AST_SYMBOL_LIT_FALSE:
		case AST_SYMBOL_LIT_TRUE:
		case AST_SYMBOL_LIT_CHAR:
		case AST_SYMBOL_LIT_STRING:
		case AST_SYMBOL:
			tempTac = tacCreate(TAC_SYMBOL, node->symbol, 0, 0);
			break;
		case AST_DEF_F:
			tempTac = tacFunc(node->symbol, code[3]);
			break;
	//	case AST_DECL_LOC:
	//		tempTac = code[0];
	//		break;
		case AST_LIST_P:
			tempTac = code[0];
			break;
		case AST_IF:
			tempTac = tacIf(code[0], code[1]);
			break;
		case AST_IF_ELSE:
			tempTac = tacIfElse(code[0], code[1], code[2]);
			break;
		case AST_LOOP:
			tempTac = tacLoop(code[0], code[1]);
			break;
		case AST_OP_SUM:
			tempTac = tacBinOp(TAC_OP_SUM, code[0], code[1]);
			break;
		case AST_OP_SUB:
			tempTac = tacBinOp(TAC_OP_SUB, code[0], code[1]);
			break;
		case AST_OP_MUL:
			tempTac = tacBinOp(TAC_OP_MUL, code[0], code[1]);
			break;
		case AST_OP_DIV:
			tempTac = tacBinOp(TAC_OP_DIV, code[0], code[1]);
			break;
		case AST_PARENTESIS:
			tempTac = code[0];
			break;
		case AST_SYMBOL_VEC:
			tempTac = tacArr(node->symbol, code[0]);
			break;
		case AST_OP_LE:
			tempTac = tacBinOp(TAC_OP_LE, code[0], code[1]);
			break;
		case AST_OP_GE:
			tempTac = tacBinOp(TAC_OP_GE, code[0], code[1]);
			break;
		case AST_OP_GRE:
			tempTac = tacBinOp(TAC_OP_GRE, code[0], code[1]);
			break;
		case AST_OP_LES:
			tempTac = tacBinOp(TAC_OP_LES, code[0], code[1]);
			break;
		case AST_OP_EQ:
			tempTac = tacBinOp(TAC_OP_EQ, code[0], code[1]);
			break;
		case AST_OP_NE:
			tempTac = tacBinOp(TAC_OP_NE, code[0], code[1]);
			break;
		case AST_OP_AND:
			tempTac = tacBinOp(TAC_OP_AND, code[0], code[1]);
			break;
		case AST_OP_OR:
			tempTac = tacBinOp(TAC_OP_OR, code[0], code[1]);
			break;
		case AST_POINTER_ADDRESS:
			tempTac = tacBinOp(TAC_POINTER_ADDRESS, code[0], 0);
			break;
		case AST_POINTER_VALUE:
			tempTac = tacBinOp(TAC_POINTER_VALUE, code[0], 0);
			break;
		case AST_CHAM_F:
			tempTac = tacCall(code[0], node->symbol);
			break;
		case AST_LIST_E:
			if (exprType == EXPR_PARAM) {
				tempTac = tacJoin(tacParam(code[0]), code[1]);
			} else if (exprType == EXPR_OUTPUT) {
				tempTac = tacJoin(tacOutput(code[0]), code[1]);
			} else {
				tempTac = tacJoin(code[0], code[1]);
			}
			break;
		case AST_COM://propagar o que vem de baixo
		case AST_BLO_COM:
			tempTac = code[0];
			break;
		case AST_SEQ:
			tempTac = tacJoin(code[0],code[1]);
			break;
		case AST_ATR_VAR:
			tempTac = tacAss(node->symbol, code[0]);
			break;
		case AST_ATR_VEC:
			tempTac = tacArrAss(node->symbol, code[0], code[1]);
			break;
		case AST_INP:
			tempTac = tacInput(node->symbol);
			break;
		case AST_OUT:
			tempTac = tacOutput(code[0]);
			break;
		case AST_RET:
			tempTac = tacReturn(code[0]);
			break;
		default:
			return 0;
			break;
	}
	return tempTac;
}

TAC_NODE * tacBinOp(int type, TAC_NODE *op1, TAC_NODE *op2) {
	TAC_NODE *tempTac;

	tempTac = tacJoin(op1, tacJoin(op2, tacCreate(type, makeTemp(), op1 ? op1->res : 0, op2 ? op2->res : 0)));

	return tempTac;
}

TAC_NODE *tacReturn(TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_RET, node->res, 0, 0);
	tempTac = tacJoin(node, tempTac);

	return tempTac;
}

TAC_NODE *tacLoop(TAC_NODE *cmd, TAC_NODE *expr) {
	HASH_NODE *labelBegin = makeLabel();
	HASH_NODE *labelEnd = makeLabel();
	TAC_NODE *jumpZero;
	TAC_NODE *jump;
	TAC_NODE *end;
	TAC_NODE *begin;

	begin = tacCreate(TAC_LABEL, labelBegin, 0, 0);
	expr->prev = begin;
	jumpZero = tacCreate(TAC_IFZERO, labelEnd, expr->res, 0);
	jumpZero->prev = expr;
	cmd = tacJoin(jumpZero, cmd);
	jump = tacCreate(TAC_LOOP, labelBegin, 0, 0);
	jump->prev = cmd;
	end = tacCreate(TAC_LABEL, labelEnd, 0, 0);
	end->prev = jump;

	return end;
}

TAC_NODE *tacCall(TAC_NODE *node, HASH_NODE* symbol) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_CHAM_F, symbol, 0, 0);
	if (node) {
		tempTac = tacJoin(tacParam(node), tempTac);
	} else {
		tempTac = tacJoin(node, tempTac);
	}
	

	return tempTac;
}

TAC_NODE *tacParam(TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_PARAM, node->res, 0, 0);
	tempTac->prev = node;

	return tempTac;
}

TAC_NODE *tacIf(TAC_NODE *expr, TAC_NODE *cmd) {
	HASH_NODE *label = makeLabel();
	TAC_NODE *jumpZero;
	TAC_NODE *end;

	jumpZero = tacCreate(TAC_IFZERO, label, expr->res, 0);
	jumpZero->prev = expr;
	cmd = tacJoin(jumpZero, cmd);
	end = tacCreate(TAC_LABEL, label, 0, 0);
	end->prev = cmd;

	return end;
}

TAC_NODE *tacIfElse(TAC_NODE *expr, TAC_NODE *cmdElse, TAC_NODE *cmdThen) {
	HASH_NODE *labelThen = makeLabel();
	HASH_NODE *labelElse = makeLabel();
	TAC_NODE *jumpZero;
	TAC_NODE *jump;
	TAC_NODE *elseNode;
	TAC_NODE *end;

	jumpZero = tacCreate(TAC_IFZERO, labelElse, expr->res, 0);
	jumpZero->prev = expr;
	cmdThen = tacJoin(jumpZero, cmdThen);
	jump = tacCreate(TAC_LOOP, labelThen, 0, 0);
	jump->prev = cmdThen;
	elseNode = tacCreate(TAC_LABEL, labelElse, 0, 0);
	elseNode->prev = jump;
	cmdElse = tacJoin(elseNode, cmdElse);
	end = tacCreate(TAC_LABEL, labelThen, 0, 0);
	end->prev = cmdElse;

	return end;
}

TAC_NODE *tacArr(HASH_NODE* symbol, TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacCreate (TAC_DECL_VEC, makeTemp(), symbol, node->res);

	return tempTac;
}

TAC_NODE *tacAss(HASH_NODE *symbol, TAC_NODE *expr) {
	TAC_NODE *tempTac = 0;

	tempTac = tacCreate(TAC_ATR_VAR, symbol, expr->res, 0);

	return tacJoin(expr, tempTac);
}

TAC_NODE *tacInput(HASH_NODE *symbol) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_INP, symbol, 0, 0);

	return tempTac;
}

TAC_NODE *tacFunc(HASH_NODE* symbol, TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacJoin(tacCreate(TAC_BEGIN_FUNC, symbol, 0, 0), tacJoin(node, tacCreate(TAC_END_FUNC, symbol, 0, 0)));

	return tempTac;
}


TAC_NODE *tacArrAss(HASH_NODE* symbol, TAC_NODE *index, TAC_NODE *expr) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_ATR_VEC, symbol, index->res, expr->res);

	return tempTac;
}

TAC_NODE *tacOutput(TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_OUT, node->res, 0, 0);
	tempTac->prev = node;

	return tempTac;
}
