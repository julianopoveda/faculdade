/*Juliano Poveda 213995
Luiz Fernando Ramos Cardozo 207113*/

#include <stdio.h>
  
int getLineNumber(void)
{
	return GlobalLineNumber;
}

int isRunning(void)
{
	return running;
}

void initMe(void)
{
	hashInit();
	running = 1;
	GlobalLineNumber = 1;
}
