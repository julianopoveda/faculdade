#include <stdio.h>
#include "hash.h"
#include "astree.h"

void semanticSetTypes(ASTREE *node);
void semanticCheckDeclarations(ASTREE *node);
