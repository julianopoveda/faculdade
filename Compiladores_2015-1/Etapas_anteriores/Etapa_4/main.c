#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	FILE* file;

	if(argc < 3){
		fprintf(stderr, "Deve ter arquivo entrada e arquivo saida\n");
		exit(1);
	}

	if(!(file = fopen(argv[1], "r")))
	{
		fprintf(stderr, "Nao foi possivel abrir o arquivo fonte %s\n", argv[1]);
		exit(2);
	}
	else
		yyin = file;

	if ((file = fopen("temp.txt", "w")) == NULL) {
        fprintf(stderr, "Impossible to create temporary file!\n");
        exit(1);
    }

	fprintf(file, "%s", argv[2]);
    fclose(file);
    
	initMe();
		
	yyparse();
	
	if(SemanticErrors!=0){
		fprintf(stderr, "Semantic errors found: %d\n", SemanticErrors);
		exit(4);
	}
	
	printf("Fonte possui %d linhas\n", getLineNumber());
	hashPrint();
	exit(0);
}
