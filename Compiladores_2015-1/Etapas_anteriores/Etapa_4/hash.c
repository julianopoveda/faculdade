/*Juliano Poveda 213995
Luiz Fernando Ramos Cardozo 207113*/
#ifndef HASH_BODY
#define HASH_BODY

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.h"

void hashInit (void)
	{
	int i;
	for(i=0; i< HASH_SIZE; i++)
		Table[i]=0;
	}
void hashPrint (void)
	{
	int i;
	HASH_NODE *n;
	for(i=0; i< HASH_SIZE; i++)
		for(n=Table[i]; n; n=n->next)
			printf("Table[%d] has %s\n", i, n->text);
	}

int hashAddress (char *text)
	{
	int address = 1;
	int i;
	
	for(i=0; i<strlen(text); ++i)
		address = (address * text[i]) % HASH_SIZE +1;
		return address-1;
	}

HASH_NODE* hashInsert(char *text, int type)
{
	HASH_NODE *newnode;
	int address;
	address = hashAddress(text);
	
	if((newnode = hashFind(text)) != 0)
		return newnode;
	
	newnode = (HASH_NODE*)calloc(1, sizeof(HASH_NODE));
	newnode->text = (char*)calloc(strlen(text)+1, sizeof(char));
	strcpy(newnode->text, text);
	newnode->type = type;
	newnode->dataType = DATATYPE_UNDEFINED;
	newnode->next = Table[address];
	Table[address] = newnode;
	newnode->paramLst = 0;
	newnode->declType = DECL_UNDEFINED;
	newnode->declCount = 0;
	
	return newnode;
}

HASH_NODE* hashFind(char* text)
{
	int address;
	HASH_NODE *node;
	address = hashAddress(text);
	for(node = Table[address]; node; node=node->next)
	{
		if(strcmp(node->text, text)==0)
			return node;
	}
	return 0;
}

#endif
