/*Juliano Poveda 213995
Luiz Fernando Ramos Cardozo 207113*/

%{
int GlobalLineNumber = 1;
int running = 1;

#include "astree.h"
#include "y.tab.h"
#include "GlobalFunctions.c"
#include "main.c"
#include <stdio.h>
#include <stdlib.h>

#define SYMBOL_UNDEFINED 0
#define SYMBOL_LIT_INTEGER 1
#define SYMBOL_LIT_FLOATING 2
#define SYMBOL_LIT_TRUE 3
#define SYMBOL_LIT_FALSE 4
#define SYMBOL_LIT_CHAR 5
#define SYMBOL_LIT_STRING 6
#define SYMBOL_IDENTIFIER 7
%}

%x COMMENT

%%
word { return KW_WORD; }
bool { return KW_BOOL; }
byte { return KW_BYTE; }
if { return KW_IF; }
then { return KW_THEN; }
else { return KW_ELSE; }
loop { return KW_LOOP; }
input { return KW_INPUT; }
output { return KW_OUTPUT; }
return { return KW_RETURN; }

[-+*,"/";:<>="\]\""\[\"")""(""{""}""!""&""$"] { return yytext[0]; }

[0-9]+ {yylval.symbol = hashInsert(yytext, LIT_INTEGER); return LIT_INTEGER; }
FALSE {yylval.symbol = hashInsert(yytext, LIT_FALSE); return LIT_FALSE; }
TRUE {yylval.symbol = hashInsert(yytext, LIT_TRUE); return LIT_TRUE; }

"<=" { return OPERATOR_LE; }
">=" { return OPERATOR_GE; }
"==" { return OPERATOR_EQ; }
"!=" { return OPERATOR_NE; }
"&&" { return OPERATOR_AND; }
"||" { return OPERATOR_OR; }


[A-Za-z_][A-Za-z_0-9]* {yylval.symbol = hashInsert(yytext, SYMBOL_IDENTIFIER); return TK_IDENTIFIER; }

\'.\' {yylval.symbol = hashInsert(yytext, LIT_CHAR); return LIT_CHAR; }
\".*\" {yylval.symbol = hashInsert(yytext, LIT_STRING); return LIT_STRING; }

\n { ++GlobalLineNumber; }
[ \t]
"//".*
"/*" { BEGIN(COMMENT); }
<COMMENT>\n { ++GlobalLineNumber; }
<COMMENT>.
<COMMENT>"*/" { BEGIN(INITIAL); }
.  {return TOKEN_ERROR;}
%%

int yywrap(void){
	running = 0;
	return 1;
}
