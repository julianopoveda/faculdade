/*Juliano Poveda 213995
Luiz Fernando Ramos Cardozo 207113*/

#ifndef HASH_HEADER
#define HASH_HEADER

#define HASH_SIZE 997

#define DECL_UNDEFINED 0
#define DECL_VAR		1
#define DECL_FUNC		2
#define DECL_VEC		3
#define DECL_PT			4


//-------
#define DATATYPE_UNDEFINED 0
#define DATATYPE_WORD 1
#define DATATYPE_BOOL 2
#define DATATYPE_BYTE 3
#define DATATYPE_POINTER 4

typedef struct hash_node
{
	char *text;
	int type;
	int dataType;
	int declType;
	int declCount;
	struct hash_node *paramLst;
	struct hash_node* next;
}HASH_NODE;

HASH_NODE *Table[HASH_SIZE];

void hashInit(void);
void hashPrint(void);
int hashAddress(char* text);
HASH_NODE* hashInsert(char* text, int type);
HASH_NODE* hashFind(char* text);
void initMe(void);

#endif
