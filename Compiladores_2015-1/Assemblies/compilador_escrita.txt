Aqui devem ir todas as declarações do assembler a medida que forem sendo geradas.
https://gcc.gnu.org/onlinedocs/5.1.0/ (Manual do gcc)
https://gcc.gnu.org/onlinedocs/gcc-5.1.0/gcc/Link-Options.html#Link-Options (seção de opções)
//Check List
variaveis globais ok
vetores globais inicializados ok
vetores globais não inicializados ok
ponteiros
funcoes sem parâmetros e sem variaveis locais
funcoes com parâmetros
funcoes com variaveis locais
funcoes com parametros e variaveis locais
if ok
if else ok
for
//Fim Check List

//Variaveis globais
	.globl	globalFloat
	.align 4
	.type	globalFloat, @object
	.size	globalFloat, 4
globalFloat:
	.long	1136033792
	.globl	globalChar
	.type	globalChar, @object
	.size	globalChar, 1
globalChar:
	.byte	99
	
//Vetor Inicializado
	.globl	numeros
	.align 16
	.type	numeros, @object
	.size	numeros, 20
numeros:
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
//Vetor não inicializado
//Funciona da seguinte forma. Inicia com 4,4 e vai crescendo de 4 em 4 e quando o número é o dobro da base muda a base até o limite de 32. 
//Ex: vetor[1] = 4,4 vetor[2] = 8,4 vetor[3] = 12,4 vetor[4] = 16,16 vetor[8] = 32,32
	.comm	numerosNInit,20,16 //Aqui representa um vetor vazio de 5 posições. Se for um de 50 o valor fica 200,32.
	
//if e else
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	//Esse é o if
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jge	.L2
	movl	$.LC0, %edi//isso deve sair. Representa o printf
	movl	$0, %eax //No if isso tem que ficar aqui
	call	printf//isso deve sair. Representa o printf
	jmp	.L3
	//Esse é o else
.L2:
	movl	$.LC1, %edi//isso deve sair. Representa o printf
	movl	$0, %eax //No else isso tem que ficar aqui
	call	printf//isso deve sair. Representa o printf
	//Esse é o bloco imediatamente após o else.
.L3:
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
//Só o if
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	//Esse é o if
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jge	.L2
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	
	
//Isso pode ser removido
.LFE2:
.size	main, .-main
.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
.section	.note.GNU-stack,"",@progbits

//Begin function
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	
//End function
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
//variavel local int
movl	$18, -12(%rbp)//18 é o valor inicializado. COmeça em -8 e vai subtraindo 4

//soma
movl	-8(%rbp), %eax
addl	-12(%rbp), %eax

//resultado
movl	%eax, -4(%rbp)
//multiplicação
movl	-32(%rbp), %eax
imull	-28(%rbp), %eax

//divisao
	movl	-16(%rbp), %eax
	cltd
	idivl	-20(%rbp)

//subtracao
	subl	-12(%rbp), %eax
	
// Declaracao de função
.globl	Parametros
	.type	Parametros, @function
Parametros:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6

//Paramtros de uma funcao
	movl	%edi, -20(%rbp) //param1
	movb	%al, -24(%rbp)//char param2
	movss	%xmm0, -28(%rbp) //float param2
	movl	%edx, -32(%rbp) //param4
	movl	%ecx, -36(%rbp)//param5
	movl	%r8d, -40(%rbp)//param6
	movl	%r9d, -44(%rbp)//param7
	
	