	.file	"operacoes_e_varlocal.c"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$18, -28(%rbp)
	movl	$35, -24(%rbp)
	movl	$5, -20(%rbp)
	movb	$99, -29(%rbp)
	movl	-28(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L2
	movl	-20(%rbp), %eax
	movl	%eax, -28(%rbp)
.L2:
	movl	-28(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jge	.L3
	movl	-24(%rbp), %eax
	movl	-28(%rbp), %edx
	addl	%edx, %eax
	addl	$20, %eax
	movl	%eax, -16(%rbp)
.L3:
	movl	-28(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jg	.L4
	movl	-28(%rbp), %eax
	subl	%eax, -24(%rbp)
.L4:
	movl	-28(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.L5
	movl	-28(%rbp), %eax
	movl	-24(%rbp), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -12(%rbp)
.L5:
	movl	-24(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jne	.L6
	movl	-24(%rbp), %eax
	cltd
	idivl	-28(%rbp)
	movl	%eax, -8(%rbp)
.L6:
	movl	-24(%rbp), %eax
	cmpl	-28(%rbp), %eax
	je	.L7
	movl	-24(%rbp), %eax
	cltd
	idivl	-28(%rbp)
	movl	%eax, -4(%rbp)
.L7:
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jg	.L8
	cmpl	$0, -28(%rbp)
	je	.L9
.L8:
	movl	-28(%rbp), %eax
	movl	-24(%rbp), %edx
	addl	%edx, %eax
	movl	%eax, -20(%rbp)
.L9:
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	setg	%dl
	cmpl	$0, -28(%rbp)
	sete	%al
	andl	%edx, %eax
	testb	%al, %al
	je	.L10
	movl	-24(%rbp), %eax
	cltd
	idivl	-28(%rbp)
	movl	%eax, -28(%rbp)
.L10:
	movl	-24(%rbp), %eax
	movl	-20(%rbp), %edx
	orl	%edx, %eax
	movl	%eax, -28(%rbp)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.globl	Parametros
	.type	Parametros, @function
Parametros:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -20(%rbp)
	movl	%esi, %eax
	movss	%xmm0, -28(%rbp)
	movl	%edx, -32(%rbp)
	movl	%ecx, -36(%rbp)
	movl	%r8d, -40(%rbp)
	movl	%r9d, -44(%rbp)
	movb	%al, -24(%rbp)
	movl	-36(%rbp), %eax
	imull	-44(%rbp), %eax
	movl	-32(%rbp), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -4(%rbp)
	movl	16(%rbp), %eax
	cltd
	idivl	-40(%rbp)
	movl	%eax, %edx
	movl	-4(%rbp), %eax
	addl	%edx, %eax
	movl	%eax, 16(%rbp)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	
	.globl	Calculos
	.type	Calculos, @function
Calculos:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$40, %rsp
	movl	$5, -32(%rbp)
	movl	$16, -28(%rbp)
	movl	$7, -24(%rbp)
	movl	$55, -20(%rbp)
	movl	$9, -16(%rbp)
	movl	$99, -12(%rbp)
	movl	$72, -8(%rbp)
	movl	-32(%rbp), %eax
	imull	-28(%rbp), %eax
	imull	-24(%rbp), %eax
	movl	%eax, -4(%rbp)
	movl	-16(%rbp), %r8d
	movl	-20(%rbp), %edi
	movl	-24(%rbp), %ecx
	movl	-28(%rbp), %edx
	movl	-32(%rbp), %eax
	movl	-12(%rbp), %esi
	movl	%esi, (%rsp)
	movl	%r8d, %r9d
	movl	%edi, %r8d
	movss	.LC0(%rip), %xmm0
	movl	$99, %esi
	movl	%eax, %edi
	call	Parametros
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	Calculos, .-Calculos
	.section	.rodata
	.align 4
.LC0:
	.long	1077936128
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
