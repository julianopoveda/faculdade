#include <stdio.h>
#include <stdlib.h>
#include "tac.h"

TAC_NODE *tacCreate(int type, HASH_NODE *res, HASH_NODE *op1, HASH_NODE *op2) {
	TAC_NODE *node;
	node = (TAC_NODE*)calloc(1, sizeof(TAC_NODE));
	node->type = type;
	node->res = res;
	node->op1 = op1;
	node->op2 = op2;
	node->prev = 0;
	node->next = 0;
	
	//tacPrintSingle(node);//remover
	return node;
}

void tacPrintSingle(TAC_NODE *node) {
	
	if(!node) return;

	if (node->type != TAC_SYMBOL) {
		fprintf(stderr, "(");
	}
	switch(node->type) {
		case TAC_OP_SUM:			fprintf(stderr, "TAC_OP_SUM"); break;
		case TAC_OP_SUB:			fprintf(stderr, "TAC_OP_SUB"); break;
		case TAC_OP_MUL:			fprintf(stderr, "TAC_OP_MUL"); break;
		case TAC_OP_DIV:			fprintf(stderr, "TAC_OP_DIV"); break;
		case TAC_OP_GRE:			fprintf(stderr, "TAC_OP_GRE"); break;
		case TAC_OP_LES:			fprintf(stderr, "TAC_OP_LES"); break;
		case TAC_OP_LE:				fprintf(stderr, "TAC_OP_LE"); break;
		case TAC_OP_GE:				fprintf(stderr, "TAC_OP_GE"); break;
		case TAC_OP_EQ:				fprintf(stderr, "TAC_OP_EQ"); break;
		case TAC_OP_NE:				fprintf(stderr, "TAC_OP_NE"); break;
		case TAC_OP_OR:				fprintf(stderr, "TAC_OP_OR"); break;
		case TAC_OP_AND:			fprintf(stderr, "TAC_OP_AND"); break;
		case TAC_RET:				fprintf(stderr, "TAC_RET"); break;
		case TAC_SYMBOL:			/*fprintf(stderr, "TAC_SYMBOL"); */break;
		case TAC_LOOP:				fprintf(stderr, "TAC_LOOP"); break;
		case TAC_IFZERO:			fprintf(stderr, "TAC_IFZERO"); break;
		case TAC_LABEL:				fprintf(stderr, "TAC_LABEL"); break;
		case TAC_CHAM_F:			fprintf(stderr, "TAC_CHAM_F"); break;
		case TAC_DECL_VEC:			fprintf(stderr, "TAC_DECL_VEC"); break;
		case TAC_ATR_VEC:			fprintf(stderr, "TAC_ATR_VEC"); break;
		case TAC_ATR_VAR:			fprintf(stderr, "TAC_ATR_VAR"); break;
		case TAC_POINTER_VALUE:		fprintf(stderr, "TAC_POINTER_VALUE"); break;
		case TAC_POINTER_ADDRESS:	fprintf(stderr, "TAC_POINTER_ADDRESS"); break;
		case TAC_OUT:				fprintf(stderr, "TAC_OUT"); break;
		case TAC_INP:				fprintf(stderr, "TAC_INP"); break;
		case TAC_LIST_E:			fprintf(stderr, "TAC_LIST_E"); break;
		case TAC_BEGIN_FUNC:		fprintf(stderr, "TAC_BEGIN_FUNC"); break;
		case TAC_END_FUNC:			fprintf(stderr, "TAC_END_FUNC"); break;
		case TAC_PARAM:				fprintf(stderr, "TAC_PARAM"); break;
		default:					fprintf(stderr, "UNDEFINED %d", node->type); break;
	}
	
	if (node->type != TAC_SYMBOL) {
		if (node->res) fprintf(stderr, ", %s", node->res->text); else fprintf(stderr, ", -");
		if (node->op1) fprintf(stderr, ", %s", node->op1->text); else fprintf(stderr, ", -");
		if (node->op2) fprintf(stderr, ", %s", node->op2->text); else fprintf(stderr, ", -");
		fprintf(stderr, ");\n");
	}
}

void tacPrint(TAC_NODE *first) {
	TAC_NODE *node;

	for (node = first; node; node = node->next) {
		tacPrintSingle(node);
	}
}

TAC_NODE *tacJoin(TAC_NODE *l1, TAC_NODE *l2) {
	TAC_NODE *node;

	if (!l1) return l2;

	if (!l2) return l1;
	
	for (node = l2; node->prev; node = node->prev);
	node->prev = l1;
	
	return l2;
}

TAC_NODE *tacInvert(TAC_NODE *list) {
	TAC_NODE *node = list;
	tacPrint(list);//remover
	
	if (node) {
		for (node = list; node->prev; node = node->prev) {
			node->prev->next = node;
		}
	}
	puts("fim teste");//remover
	return node;
}

TAC_NODE *makeTac(ASTREE *node, int exprType) {
	int i;
	int tempExpr = exprType;
	TAC_NODE *tempTac;
	TAC_NODE *code[MAX_SONS];

	if (!node) return 0;

	switch (node->type) {
		case AST_CHAM_F:
			tempExpr = EXPR_PARAM;
			break;
		case AST_OUT:
			tempExpr = EXPR_OUTPUT;
			break;
		default:
			break;
	}

	for (i = 0; i < MAX_SONS; ++i) {
		code[i] = makeTac(node->son[i], tempExpr);
	}
	
	switch (node->type) {
		case AST_PROG: //AST_DECL: 
			tempTac = tacJoin(code[0],code[1]);
			break;
		case AST_SYMBOL_LIT_INT://AST_LIT_INTEGER:
		case AST_SYMBOL_LIT_FALSE: //AST_LIT_FALSE:
		case AST_SYMBOL_LIT_TRUE://AST_LIT_TRUE:
		case AST_SYMBOL_LIT_CHAR://AST_LIT_CHAR:
		case AST_SYMBOL_LIT_STRING://AST_LIT_STRING:
		case AST_SYMBOL://AST_SYMBOL:
			tempTac = tacCreate(TAC_SYMBOL, node->symbol, 0, 0);
			break;
		case AST_DEF_F://AST_FUNC: 
			tempTac = tacFunc(node->symbol, code[2]);
			break;
		case AST_IF://AST_IF
			tempTac = tacIf(code[0], code[1]);
			break;
		case AST_IF_ELSE://AST_IF_ELSE
			tempTac = tacIfElse(code[0], code[1], code[2]);
			break;
		case AST_LOOP://AST_LOOP
			tempTac = tacLoop(code[0], code[1]);
			break;
		case AST_OP_SUM:
			tempTac = tacBinOp(TAC_OP_SUM, code[0], code[1]);
			break;
		case AST_OP_SUB:
			tempTac = tacBinOp(TAC_OP_SUB, code[0], code[1]);
			break;
		case AST_OP_MUL:
			tempTac = tacBinOp(TAC_OP_MUL, code[0], code[1]);
			break;
		case AST_OP_DIV:
			tempTac = tacBinOp(TAC_OP_DIV, code[0], code[1]);
			break;
		case AST_PARENTESIS://AST_PARENT:
			tempTac = code[0];
			break;
		case AST_SYMBOL_VEC://AST_ARR:
			tempTac = tacArr(node->symbol, code[0]);
			break;
		case AST_OP_LE:
			tempTac = tacBinOp(TAC_OP_LE, code[0], code[1]);
			break;
		case AST_OP_GE:
			tempTac = tacBinOp(TAC_OP_GE, code[0], code[1]);
			break;
		case AST_OP_GRE:
			tempTac = tacBinOp(TAC_OP_GRE, code[0], code[1]);
			break;
		case AST_OP_LES:
			tempTac = tacBinOp(TAC_OP_LES, code[0], code[1]);
			break;
		case AST_OP_EQ:
			tempTac = tacBinOp(TAC_OP_EQ, code[0], code[1]);
			break;
		case AST_OP_NE:
			tempTac = tacBinOp(TAC_OP_NE, code[0], code[1]);
			break;
		case AST_OP_AND:
			tempTac = tacBinOp(TAC_OP_AND, code[0], code[1]);
			break;
		case AST_OP_OR:
			tempTac = tacBinOp(TAC_OP_OR, code[0], code[1]);
			break;
		case AST_POINTER_ADDRESS:
			tempTac = tacBinOp(TAC_POINTER_ADDRESS, code[0], 0);
			break;
		case AST_POINTER_VALUE:
			tempTac = tacBinOp(TAC_POINTER_VALUE, code[0], 0);
			break;
		case AST_CHAM_F://AST_CALL:
			tempTac = tacCall(code[0], node->symbol);
			break;
		case AST_LIST_E://AST_EXPRLST:
			if (exprType == EXPR_PARAM) {
				tempTac = tacJoin(tacParam(code[0]), code[1]);
			} else if (exprType == EXPR_OUTPUT) {
				tempTac = tacJoin(tacOutput(code[0]), code[1]);
			} else {
				tempTac = tacJoin(code[0], code[1]);
			}
			break;
		case AST_BLO_COM://AST_BLOCK:
			tempTac = code[0];
			break;
		case AST_SEQ://AST_LCMD:
			tempTac = tacJoin(code[0],code[1]);
			break;
		case AST_ATR_VAR://AST_ASS:
			tempTac = tacAss(node->symbol, code[0]);
			break;
		case AST_ATR_VEC://AST_ARR_ASS:
			tempTac = tacArrAss(node->symbol, code[0], code[1]);
			break;
		case AST_INP://AST_INPUT:
			tempTac = tacInput(node->symbol);
			break;
		case AST_OUT:
			tempTac = tacOutput(code[0]);
			break;
		case AST_RET:// AST_RETURN:
			tempTac = tacReturn(code[0]);
			break;
		default:
			return 0;
			break;
	}
	//tacPrintSingle(tempTac);//remover
	return tempTac;
}

TAC_NODE * tacBinOp(int type, TAC_NODE *op1, TAC_NODE *op2) {
	TAC_NODE *tempTac;

	tempTac = tacJoin(op1, tacJoin(op2, tacCreate(type, makeTemp(), op1 ? op1->res : 0, op2 ? op2->res : 0)));

	return tempTac;
}

TAC_NODE *tacReturn(TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_RET, node->res, 0, 0);
	tempTac = tacJoin(node, tempTac);

	return tempTac;
}

TAC_NODE *tacLoop(TAC_NODE *cmd, TAC_NODE *expr) {
	HASH_NODE *labelBegin = makeLabel();
	HASH_NODE *labelEnd = makeLabel();
	TAC_NODE *jumpZero;
	TAC_NODE *jump;
	TAC_NODE *end;
	TAC_NODE *begin;

	begin = tacCreate(TAC_LABEL, labelBegin, 0, 0);
	expr->prev = begin;
	jumpZero = tacCreate(TAC_IFZERO, labelEnd, expr->res, 0);
	jumpZero->prev = expr;
	cmd = tacJoin(jumpZero, cmd);
	jump = tacCreate(TAC_LOOP, labelBegin, 0, 0);
	jump->prev = cmd;
	end = tacCreate(TAC_LABEL, labelEnd, 0, 0);
	end->prev = jump;

	return end;
}

TAC_NODE *tacCall(TAC_NODE *node, HASH_NODE* symbol) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_CHAM_F, symbol, 0, 0);
	if (node) {
		tempTac = tacJoin(tacParam(node), tempTac);
	} else {
		tempTac = tacJoin(node, tempTac);
	}
	

	return tempTac;
}

TAC_NODE *tacParam(TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_PARAM, node->res, 0, 0);
	tempTac->prev = node;

	return tempTac;
}

TAC_NODE *tacIf(TAC_NODE *expr, TAC_NODE *cmd) {
	HASH_NODE *label = makeLabel();
	TAC_NODE *jumpZero;
	TAC_NODE *end;

	jumpZero = tacCreate(TAC_IFZERO, label, expr->res, 0);
	jumpZero->prev = expr;
	cmd = tacJoin(jumpZero, cmd);
	end = tacCreate(TAC_LABEL, label, 0, 0);
	end->prev = cmd;

	return end;
}

TAC_NODE *tacIfElse(TAC_NODE *expr, TAC_NODE *cmdElse, TAC_NODE *cmdThen) {
	HASH_NODE *labelThen = makeLabel();
	HASH_NODE *labelElse = makeLabel();
	TAC_NODE *jumpZero;
	TAC_NODE *jump;
	TAC_NODE *elseNode;
	TAC_NODE *end;

	jumpZero = tacCreate(TAC_IFZERO, labelElse, expr->res, 0);
	jumpZero->prev = expr;
	cmdThen = tacJoin(jumpZero, cmdThen);
	jump = tacCreate(TAC_LOOP, labelThen, 0, 0);
	jump->prev = cmdThen;
	elseNode = tacCreate(TAC_LABEL, labelElse, 0, 0);
	elseNode->prev = jump;
	cmdElse = tacJoin(elseNode, cmdElse);
	end = tacCreate(TAC_LABEL, labelThen, 0, 0);
	end->prev = cmdElse;

	return end;
}

TAC_NODE *tacArr(HASH_NODE* symbol, TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacCreate (TAC_DECL_VEC, makeTemp(), symbol, node->res);

	return tempTac;
}

TAC_NODE *tacAss(HASH_NODE *symbol, TAC_NODE *expr) {
	TAC_NODE *tempTac = 0;

	tempTac = tacCreate(TAC_ATR_VAR, symbol, expr->res, 0);

	return tacJoin(expr, tempTac);
}

TAC_NODE *tacInput(HASH_NODE *symbol) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_INP, symbol, 0, 0);

	return tempTac;
}

TAC_NODE *tacFunc(HASH_NODE* symbol, TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacJoin(tacCreate(TAC_BEGIN_FUNC, symbol, 0, 0), tacJoin(node, tacCreate(TAC_END_FUNC, symbol, 0, 0)));

	return tempTac;
}


TAC_NODE *tacArrAss(HASH_NODE* symbol, TAC_NODE *index, TAC_NODE *expr) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_ATR_VEC, symbol, index->res, expr->res);

	return tempTac;
}

TAC_NODE *tacOutput(TAC_NODE *node) {
	TAC_NODE *tempTac;

	tempTac = tacCreate(TAC_OUT, node->res, 0, 0);
	tempTac->prev = node;

	return tempTac;
}

//ok
void tacGetOperators(TAC_NODE *node, FILE *file) {
	if (node->op1->type == SYMBOL_LIT_INTEGER){
		
		//fprintf(file, "\tmovl\t$%d, %%eax\n", atoi(node->op1->text));
		fprintf(file, "movl	$%d, %%eax\n", atoi(node->op1->text));
	} else if (node->op1->type == SYMBOL_LIT_CHAR) {
		
		fprintf(file, "movb	$%d, %%eax\n", node->op1->text[1]);
		//fprintf(file, "\tmovl\t$%d, %%eax\n", node->op1->text[1]);
	} else {
		fprintf(file,"movl %s, %%eax\n", node->op1->text);
	}

	if (node->op2->type == SYMBOL_LIT_INTEGER) {
		fprintf(file, "movl	$%d, %%eax\n", atoi(node->op2->text));
	} else if(node->op2->type == SYMBOL_LIT_CHAR) {
		fprintf(file, "movb	$%d, %%eax\n", node->op2->text[1]);
	} else {
		fprintf(file, "movl %s, %%edx\n", node->op2->text);
	}
}

void tacToAssembly(TAC_NODE *list, FILE *file) {
	TAC_NODE *node;

	for (node = list; node; node = node->next) {
		switch(node->type) {
			case TAC_OP_SUM:
				//fprintf(stderr, "TAC_ADD\n");
				tacGetOperators(node, file);
				//fprintf(file, "\taddl\t%%edx, %%eax\n");
				//fprintf(file, "\tmovl\t%%eax, %s\n", node->res->text);
//				puts("entrei");
				fprintf(file, "addl %%edx, %%eax\n");
				fprintf(file, "movl %%eax, %s\n", node->res->text);
				break;
			case TAC_OP_SUB:
				//fprintf(stderr, "TAC_SUB\n");
				tacGetOperators(node, file);
				fprintf(file, "subl %%edx, %%eax\n");
				fprintf(file, "movl %%eax, %s\n", node->res->text);
				break;
			case TAC_OP_MUL:
				//fprintf(stderr, "TAC_MUL\n");
				tacGetOperators(node, file);
				fprintf(file, "imull %%edx, %%eax\n");
				fprintf(file, "movl %%eax, %s\n", node->res->text);
				break;
			case TAC_OP_DIV:
				//fprintf(stderr, "TAC_DIV\n");
				tacGetOperators(node, file);
				/*fprintf(file, "\tmovl\t%%edx, -4(%%rbp)\n");
				fprintf(file, "\tmovl\t%%eax, %%edx\n");
				fprintf(file, "\tsarl\t$31, %%edx\n");
				fprintf(file, "\tidivl\t-4(%%rbp)\n");
				fprintf(file, "\tmovl\t%%eax, %s\n", node->res->text);*/
				fprintf(file, "movl %%eax, -4(%%rbp)\n");
				fprintf(file, "cltd\n");
				fprintf(file, "idivl -8(%%rbp)\n");
				fprintf(file, "movl %%eax, %s\n", node->res->text);
				break;
			case TAC_OP_GRE:
				//fprintf(stderr, "TAC_GT\n");
				tacGetOperators(node, file);
				fprintf(file, "cmpl %%eax, %%edx\n");
				//fprintf(file, "setgt%%al\n");
				//fprintf(file, "\tmovzbl\t%%al, %%eax\n");
				fprintf(file, "jg %s\n", node->res->text);
				break;
			case TAC_OP_LES:
				//fprintf(stderr, "TAC_LT\n");
				tacGetOperators(node, file);
				fprintf(file, "cmpl %%eax, %%edx\n");
				//fprintf(file, "\tsetl\t%%al\n");
				//fprintf(file, "\tmovzbl\t%%al, %%eax\n");
				fprintf(file, "jl %s\n", node->res->text);
				break;
			case TAC_OP_LE:
				//fprintf(stderr, "TAC_LE\n");
				tacGetOperators(node, file);
				fprintf(file, "cmpl %%eax, %%edx\n");
				//fprintf(file, "\tsetle\t%%al\n");
				//fprintf(file, "\tmovzbl\t%%al, %%eax\n");
				fprintf(file, "jle %s\n", node->res->text);
				break;
			case TAC_OP_GE:
				//fprintf(stderr, "TAC_GE\n");
				tacGetOperators(node, file);
				fprintf(file, "cmpl %%eax, %%edx\n");
				//fprintf(file, "\tsetge\t%%al\n");
				//fprintf(file, "\tmovzbl\t%%al, %%eax\n");
				fprintf(file, "jge %s\n", node->res->text);
				break;
			case TAC_OP_EQ:
				//fprintf(stderr, "TAC_EQ\n");
				tacGetOperators(node, file);
				fprintf(file, "cmpl %%eax, %%edx\n");
				//fprintf(file, "\tsete\t%%al\n");
				//fprintf(file, "\tmovzbl\t%%al, %%eax\n");
				fprintf(file, "je %s\n", node->res->text);
				break;
			case TAC_OP_NE:
				//fprintf(stderr, "TAC_NE\n");
				tacGetOperators(node, file);
				fprintf(file, "cmpl %%eax, %%edx\n");
				//fprintf(file, "\tsetne\t%%al\n");
				//fprintf(file, "\tmovzbl\t%%al, %%eax\n");
				fprintf(file, "jne %s\n", node->res->text);
				break;
			case TAC_OP_OR:
				//fprintf(stderr, "TAC_OR\n");
				tacGetOperators(node, file);
				fprintf(file, "orl %%eax, %%edx\n");
				fprintf(file, "movl %%eax, %s\n", node->res->text);
				break;
			case TAC_OP_AND:
				//fprintf(stderr, "TAC_AND\n");
				tacGetOperators(node, file);
				fprintf(file, "andl %%eax, %%edx\n");
				fprintf(file, "movl %%eax, %s\n", node->res->text);
				break;
			case TAC_RET:
				//fprintf(stderr, "TAC_RETURN\n");
				switch (node->op1->type) {
					case SYMBOL_LIT_INTEGER:
						fprintf(file, "movl $%d, %%eax\n", atoi(node->res->text));
						break;
					case SYMBOL_LIT_TRUE:
						fprintf(file, "movl $%d, %%eax\n", 1);
						break;
					case SYMBOL_LIT_FALSE:
						fprintf(file, "movl $%d, %%eax\n", 0);
						break;
					case SYMBOL_LIT_CHAR:
						fprintf(file, "movl $%d, %%eax\n", node->res->text[1]);
						break;
					case SYMBOL_IDENTIFIER:
						fprintf(file, "movl %s, %%eax\n", node->res->text);
						break;
					default:
						break;
				}
				break;//OK
			case TAC_SYMBOL:
				//fprintf(stderr, "TAC_SYMBOL\n");
				break;
			case TAC_LOOP://fazer
				//fprintf(stderr, "TAC_JUMP\n");
				fprintf(file, "jmp %s\n", node->res->text);
				break;
			case TAC_IFZERO://feito
				//fprintf(stderr, "TAC_IFZ\n");
				switch (node->prev->type) {
					case TAC_OP_GRE:
						fprintf(file, "jge %s\n", node->res->text); 
						break;
					case TAC_OP_LES:
						fprintf(file, "jle %s\n", node->res->text);
						break;
					case TAC_OP_LE:
						fprintf(file, "jl %s\n", node->res->text);
						break;
					case TAC_OP_GE:
						fprintf(file, "jg %s\n", node->res->text);
						break;
					case TAC_OP_EQ:
						fprintf(file, "jne %s\n", node->res->text);
						break;
					case TAC_OP_NE:
						fprintf(file, "je %s\n", node->res->text);
						break;
					case TAC_OP_OR:
						fprintf(file, "jz %s\n", node->res->text);
						break;
					case TAC_OP_AND:
						fprintf(file, "jz %s\n", node->res->text);
						break;
					case TAC_SYMBOL:
						fprintf(file, "jnz %s\n", node->res->text);
						break;
					default:
						break;
				}
				break;
			case TAC_LABEL://feito
				//fprintf(stderr, "TAC_LABEL\n");
				fprintf(file, "%s:\n", node->res->text);
				break;
			case TAC_CHAM_F://melhorar
				//fprintf(stderr, "TAC_CALL\n");
				fprintf(file, "movl $%s, %%eax\n", node->res->paramLst->text);
				fprintf(file, "call %s\n", node->res->text);
				break;
			case TAC_DECL_VEC:
				//fprintf(stderr, "TAC_ARR\n");
				if (atoi(node->op2->text) > 0) {
					//fprintf(file, ".globl %s\n", node->res->text);
					//fprintf(file, ".align 16\n");
					//fprintf(file, ".type %s, @object\n", node->res->text);
					//fprintf(file, ".size %s, %d\n", node->res->text, atoi(node->op2->text) * 4); 
					//fprintf(file, "%s: .long %d\n", node->res->text, atoi(node->op1->text));
					
					fprintf(file, "movl %s+%d, %%eax\n", node->op1->text, atoi(node->op2->text) * 4);
					fprintf(file, "movl %%eax, %s\n", node->res->text);
				} else {
					fprintf(file, "movl %s, %%eax\n", node->op1->text);
					fprintf(file, "movl %%eax, %s\n", node->res->text);
				}
				break;
			case TAC_ATR_VEC:
				//fprintf(stderr, "TAC_ARR_ASS\n");
				switch (node->op2->type) {
					case SYMBOL_LIT_INTEGER:
						if (atoi(node->op1->text) > 0) {
							fprintf(file, "movl $%d, %s+%d\n", atoi(node->op2->text), node->res->text, atoi(node->op1->text) * 4);
						} else {
							fprintf(file, "movl $%d, %s\n", atoi(node->op2->text), node->res->text);
						}
						break;
					case SYMBOL_LIT_TRUE:
						if (atoi(node->op1->text) > 0) {
							fprintf(file, "movl $%d, %s+%d\n", 1, node->res->text, atoi(node->op1->text) * 4);
						} else {
							fprintf(file, "movl $%d, %s\n", 1, node->res->text);
						}
						break;
					case SYMBOL_LIT_FALSE:
						if (atoi(node->op1->text) > 0) {
							fprintf(file, "movl $%d, %s+%d\n", 0, node->res->text, atoi(node->op1->text) * 4);
						} else {
							fprintf(file, "movl $%d, %s\n", 0, node->res->text);
						}
						break;
					case SYMBOL_LIT_CHAR:
						if (atoi(node->op1->text) > 0) {
							fprintf(file, "movl $%d, %s+%d\n", node->op2->text[1], node->res->text, atoi(node->op1->text) * 4);
						} else {
							fprintf(file, "movl $%d, %s\n", node->op2->text[1], node->res->text);
						}
						break;
					case SYMBOL_IDENTIFIER:
						if (node->op2->declType == DECL_FUNC) {
							if (atoi(node->op1->text) > 0) {
								fprintf(file, "movl %%edx, %s+%d\n", node->res->text, atoi(node->op1->text) * 4);               
							} else {
								fprintf(file, "movl %%edx, %s\n", node->res->text);
							}
						} else {
							if (atoi(node->op1->text) > 0) {
								fprintf(file, "movl %s, %%eax\n", node->op2->text);
								fprintf(file, "movl %%eax, %s+%d\n", node->res->text, atoi(node->op1->text) * 4);
								fprintf(file, "movl %s+%d, %%edx\n", node->res->text, atoi(node->op1->text) * 4);
							} else {
								fprintf(file, "movl %s, %%eax\n", node->op2->text);
								fprintf(file, "movl %%eax, %s\n", node->res->text);
								fprintf(file, "movl %s, %%edx\n", node->res->text);
							}
						}
						break;
					default:
						break;
				}
				break;
			case TAC_ATR_VAR:
				//fprintf(stderr, "TAC_ASS\n");
				switch (node->op1->type) {
					case SYMBOL_LIT_INTEGER:
						fprintf(file, "movl $%d, %s\n", atoi(node->op1->text), node->res->text);
						break;
					case SYMBOL_LIT_TRUE:
						fprintf(file, "movl $%d, %s\n", 1, node->res->text);
						break;
					case SYMBOL_LIT_FALSE:
						fprintf(file, "movl $%d, %s\n", 0, node->res->text);
						break;
					case SYMBOL_LIT_CHAR:
						fprintf(file, "movb $%d, %s\n", node->op1->text[1], node->res->text);
						break;
					case SYMBOL_IDENTIFIER:
						fprintf(file, "movl %s, %%eax\n",node->op1->text);
						fprintf(file, "movl %%eax, %s\n",node->res->text);
						fprintf(file, "movl %s, %%edx\n",node->res->text);
						break;
					default:
						break;
				}
				break;
			case TAC_POINTER_VALUE:
				//fprintf(stderr, "TAC_DREF\n");
				//fprintf(file, "\tmovq\t%s(%%rip), %%rax\n", node->op1->text);
				//fprintf(file, "\tmovl\t(%%rax), %%eax\n");
				//fprintf(file, "\tmovl\t%%eax, %s(%%rip)\n", node->res->text);
				break;
			case TAC_POINTER_ADDRESS:
				//fprintf(stderr, "TAC_REF\n");
				//fprintf(file, "\tmovq\t$%s, %s(%%rip)\n", node->res->text, node->op1->text);
				break;
			case TAC_OUT:
				//fprintf(stderr, "TAC_OUTPUT\n");
				switch (node->res->type) {
					case SYMBOL_LIT_INTEGER:
						fprintf(file, "movlm$%d, %%edx\n", atoi(node->res->text));
						fprintf(file, "movl$__OUTPUT, %%eax\n");
						fprintf(file, "movl	%%edx, %%esi\n");
						break;
					case SYMBOL_LIT_TRUE:
						fprintf(file, "movl	$%d, %%eax\n", 1);
						break;
					case SYMBOL_LIT_FALSE:
						fprintf(file, "movl	$%d, %%eax\n", 0);
						break;
					case SYMBOL_LIT_CHAR:
						fprintf(file, "movl	$%d, %%edx\n", node->res->text[1]);
						fprintf(file, "movl	$__OUTPUT, %%eax\n");
						fprintf(file, "movl	%%edx, %%esi\n");
						break;
					case SYMBOL_LIT_STRING:
						fprintf(file, "movl	$%s, %%eax\n", node->res->text);
						break;
					case SYMBOL_IDENTIFIER:
						fprintf(file, "movl	%s, %%edx\n", node->res->text);
						fprintf(file, "movl	$__OUTPUT, %%eax\n");
						fprintf(file, "movl	%%edx, %%esi\n");
						break;
					default:
						break;
				}
				fprintf(file, "movq %%rax, %%rdi\n");
				fprintf(file, "movl $0, %%eax\n");
				fprintf(file, "call printf\n");
				break;
			case TAC_INP:
				//fprintf(stderr, "TAC_INPUT\n");
				break;
			case TAC_LIST_E:
				//fprintf(stderr, "TAC_EXPRLST\n");
				break;
			case TAC_BEGIN_FUNC://feito
				//fprintf(stderr, "TAC_BEGINFUN\n");
				fprintf(file, ".globl %s\n", node->res->text);
				fprintf(file, ".type %s, @function\n", node->res->text);
				fprintf(file, "%s:\n", node->res->text);
				fprintf(file, ".cfi_startproc\n");
				fprintf(file, "pushq %%rbp\n");
				fprintf(file, ".cfi_def_cfa_offset 16\n");
				fprintf(file, ".cfi_offset 6, -16\n");
				fprintf(file, "movq %%rsp, %%rbp\n");
				fprintf(file, ".cfi_def_cfa_register 6\n");
				break;
			case TAC_END_FUNC://feito
				//fprintf(stderr, "TAC_ENDFUN\n");
				fprintf(file, "popq %%rbp\n");
				fprintf(file, ".cfi_def_cfa 7, 8\n");
				fprintf(file, "ret\n");
				fprintf(file, ".cfi_endproc\n");
				break;
			case TAC_PARAM:
				//fprintf(stderr, "TAC_PARAM\n");
				break;
			default:
				//fprintf(stderr, "UNDEFINED %d", node->type);
				break;
		}
	}
}

void writeAssembly(TAC_NODE *list, FILE *file) {
	fprintf(file, ".file \"out.c\"\n");
	hashToAssembly(file);
	//fprintf(file, "\t.section\t.rodata\n");
	//fprintf(file, "__OUTPUT:\n");
	//fprintf(file, "\t.string \"%%d\"\n");
	fprintf(file, ".text\n");
	tacToAssembly(list, file);
	fprintf(file, ".ident \"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2\"\n");
	fprintf(file, ".section .note.GNU-stack,\"\",@progbits\n");
}