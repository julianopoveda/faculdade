/*Juliano Poveda 213995
Luiz Fernando Ramos Cardozo 207113*/
#ifndef HASH_BODY
#define HASH_BODY

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.h"

void hashInit (void)
	{
	int i;
	for(i=0; i< HASH_SIZE; i++)
		Table[i]=0;
		
	for(i=0; i< MAX_BUFFER; i++){
		tempBuffer[i] = ' ';
		labelBuffer[i] = ' ';
		stringBuffer[i] = ' ';
	}
	SemanticErrors = 0;
	nextTemp = 0 ;
	nextLabel = 0;
	nextString = 0;
	}
void hashPrint (void)
	{
	int i;
	HASH_NODE *n;
	for(i=0; i< HASH_SIZE; i++)
		for(n=Table[i]; n; n=n->next)
			printf("Table[%d] has %s\n", i, n->text);
	}

int hashAddress (char *text)
	{
	int address = 1;
	int i;
	
	for(i=0; i<strlen(text); ++i)
		address = (address * text[i]) % HASH_SIZE +1;
		return address-1;
	}

HASH_NODE* hashInsert(char *text, int type)
{
	HASH_NODE *newnode;
	int address;
	address = hashAddress(text);
	
	//verifica se ja esta na tabela
	if((newnode = hashFind(text)) != 0)
		return newnode;
	
	newnode = (HASH_NODE*)calloc(1, sizeof(HASH_NODE));
	newnode->text = (char*)calloc(strlen(text)+1, sizeof(char));
	strcpy(newnode->text, text);
	newnode->type = type;
	newnode->dataType = DATATYPE_UNDEFINED;
	newnode->next = Table[address];
	Table[address] = newnode;
	//-------
	newnode->paramLst = 0;
	newnode->declType = DECL_UNDEFINED;
	newnode->declCount = 0;
   //---------
	return newnode;
}

HASH_NODE* hashFind(char* text)
{
	int address;
	HASH_NODE *node;
	address = hashAddress(text);
	for(node = Table[address]; node; node=node->next)
	{
		if(strcmp(node->text, text)==0)
			return node;
	}
	return 0;//se nao achou o token na lista retorna 0
}

//------inserções
HASH_NODE *makeTemp(void) {
	sprintf(tempBuffer, "__temp%d", nextTemp);
	++nextTemp;

	return hashInsert(tempBuffer, SYMBOL_IDENTIFIER);
}

HASH_NODE *makeLabel(void) {
	sprintf(labelBuffer,"__label%d",nextLabel);
	++nextLabel;

	return hashInsert(labelBuffer, SYMBOL_LABEL);
}

void hashToAssembly(FILE *file) {
	int i;
	HASH_NODE *node;
	HASH_NODE *tempNode;
	char stringLabel[32];

	for (i = 0; i < HASH_SIZE; ++i) {
		for (node = Table[i]; node; node = node->next) {
			//printf("%s\n", node->text);
			switch (node->type) {
				case SYMBOL_LIT_INTEGER:
					break;
				case SYMBOL_LIT_TRUE:
					fprintf(file,".globl %s\n", node->text);
					fprintf(file,".data\n");
					fprintf(file,".align 4\n");
					fprintf(file,".type\t%s, @object\n", node->text);
					fprintf(file,".size\t%s, 4\n", node->text);
					fprintf(file,"%s:\n", node->text);
					fprintf(file,".long %d\n", 1);
					break;
				case SYMBOL_LIT_FALSE:
					fprintf(file,".globl %s\n", node->text);
					fprintf(file,".data\n");
					fprintf(file,".align 4\n");
					fprintf(file,".type %s, @object\n", node->text);
					fprintf(file,".size %s, 4\n", node->text);
					fprintf(file,"%s:\n", node->text);
					fprintf(file,".long %d\n", 0);
					break;
				case SYMBOL_LIT_CHAR:
					break;
				case SYMBOL_LIT_STRING:
					strcpy(stringBuffer, node->text);
					sprintf(stringLabel, "__LC%d", nextString);
					strcpy(node->text, stringLabel);
					fprintf(file,"%s:\n", stringLabel);
					fprintf(file,".string %s\n", stringBuffer);
					fprintf(file,".text\n");
					nextString++;
					break;
				case SYMBOL_IDENTIFIER:
					switch (node->declType) {
						case DECL_VAR:
							fprintf(file,".globl %s\n", node->text);
							fprintf(file,".data\n");
							fprintf(file,".align 4\n");
							fprintf(file,".type %s, @object\n", node->text);
							fprintf(file,".size %s, 4\n", node->text);
							fprintf(file,"%s:\n", node->text);
							if (node->value) {
								switch (node->dataType) {
									case DATATYPE_WORD:
										if (node->value->type == SYMBOL_LIT_INTEGER) {
											fprintf(file,".long %d\n", atoi(node->value->text));
										} else {
											// SYMBOL_LIT_CHAR
											fprintf(file,".long %d\n", node->value->text[1]);
										}
										break;
									case DATATYPE_BOOL:
										if (node->value->type == SYMBOL_LIT_TRUE) {
											fprintf(file,".long %d\n", 1);
										} else {
											// SYMBOL_LIT_FALSE
											fprintf(file,".long %d\n", 0);
										}
										break;
									case DATATYPE_BYTE:
										if (node->value->type == SYMBOL_LIT_CHAR) {
											fprintf(file,".long %d\n", node->value->text[1]);
										} else {
											// SYMBOL_LIT_INTEGER
											fprintf(file,".long %d\n", atoi(node->value->text));
										}
										break;
									case DATATYPE_POINTER:
										fprintf(file,".long %d\n", atoi(node->value->text));
										break;
									default:
										break;
								}
							} else {
								// NON-INITIALIZED VARIABLE
								fprintf(file,".long %d\n", 0);
							}
							break;
						case DECL_FUNC:
							break;
						case DECL_VEC:
							if (node->value) {
								fprintf(file, ".globl %s\n", node->text);
								fprintf(file, ".data\n");
								if (node->size <= 3) {
									fprintf(file, ".align 4\n");
								} else if (node->size <= 7) {
									fprintf(file, ".align 16\n");
								} else {
									fprintf(file, ".align 32\n");
								}
								fprintf(file, ".type %s, @object\n", node->text);
								fprintf(file, ".size %s, %d\n", node->text, node->size * 4);
								fprintf(file, "%s:\n", node->text);
								tempNode = node->value;
								while (tempNode) {
									fprintf(file, ".long %d\n", atoi(tempNode->text));
									tempNode = tempNode->value;
								}
							} else {
								if (node->size <= 3) {
									fprintf(file,".comm %s, %d, 4\n", node->text, node->size * 4);
								} else if (node->size <= 7) {
									fprintf(file,".comm %s, %d, 16\n", node->text, node->size * 4);
								} else {
									fprintf(file,".comm %s, %d, 32\n", node->text, node->size * 4);
								}
							}							
							break;
						case DECL_PT:
							fprintf(file,".globl %s\n", node->text);
							fprintf(file,".bss\n");
							fprintf(file,".align 8\n");
							fprintf(file,".type %s, @object\n", node->text);
							fprintf(file,".size %s, 8\n", node->text);
							fprintf(file,"%s:\n", node->text);
							fprintf(file,".zero 8\n");
							break;
						default:
							// It's a temporary variable
							fprintf(file,".globl %s\n", node->text);
							fprintf(file,".data\n");
							fprintf(file,".align 4\n");
							fprintf(file,".type %s, @object\n", node->text);
							fprintf(file,".size %s, 4\n", node->text);
							fprintf(file,"%s:\n", node->text);
							fprintf(file,".long %d\n", 0);
							break;
					}
			}
		}
	} 
}

#endif