%{
#include <stdio.h>
#include <stdlib.h>
#include "astree.h"

FILE *yyin;

%}

%union{
	HASH_NODE* symbol;
	ASTREE* astree;
}

%token KW_WORD
%token KW_BOOL
%token KW_BYTE
%token KW_CHAR
%token KW_IF
%token KW_THEN
%token KW_ELSE
%token KW_LOOP
%token KW_INPUT
%token KW_RETURN
%token KW_OUTPUT

%token OPERATOR_LE
%token OPERATOR_GE
%token OPERATOR_EQ
%token OPERATOR_NE
%token OPERATOR_AND
%token OPERATOR_OR

%token<symbol> TK_IDENTIFIER
%token<symbol> LIT_INTEGER
%token<symbol> LIT_FALSE
%token<symbol> LIT_TRUE
%token<symbol> LIT_CHAR
%token<symbol> LIT_STRING

%token TOKEN_ERROR

//Variaveis Tipo
%type<astree> expressao
%type<astree> lista_expressoes
%type<astree> lista_expressoes_nao_vazia
%type<astree> controle_fluxo
%type<astree> literal
%type<astree> input
%type<astree> output
%type<astree> return
%type<astree> atribuicao
%type<astree> ini_var
%type<astree> ini_vetor
%type<astree> comando
%type<astree> seq_comando
%type<astree> decl_var
%type<astree> decl_ponteiro
%type<astree> decl_vetor
%type<astree> p
%type<astree> bloco_comando
%type<astree> parametro
%type<astree> tipo_var
%type<astree> lista_parametros
%type<astree> def_funcao
%type<astree> decl_global
%type<astree> programa
%type<astree> lista_parametros_nao_vazia
%type<astree> comando_nao_vazio

%type<astree> var_local

//Fim variaveis Tipo

%left OPERATOR_OR OPERATOR_AND
%left '<' '>' OPERATOR_LE OPERATOR_GE OPERATOR_EQ OPERATOR_NE
%left '+' '-'
%left '*' '/'

%%

p : programa { 
                $$ = $1 ; 
                astreePrintCode($1); 
                semanticSetTypes($1);
                semanticCheckDeclarations($1); 
                usageCheck($1); 
		typeCheck($1);
		//tacPrint(tacInvert(makeTac($1, 0)));
        	FILE *file;
		file = fopen("out.s","w");
		writeAssembly(tacInvert(makeTac($1, 0)), file);
		fclose(file);
	
                }

programa : decl_global programa { $$ = astreeCreate(AST_PROG, 0, $1, $2, 0, 0); }
        | def_funcao programa 	{ $$ = astreeCreate(AST_PROG, 0, $1, $2, 0, 0); }
        |  			{ $$ = 0; }//astreeCreate(AST_EMPTY, 0, 0, 0, 0, 0); }
        ;

decl_global : decl_var ';'  	{ $$ = astreeCreate(AST_DECL_GL, 0, $1, 0, 0, 0); }
	| decl_ponteiro ';' 	{ $$ = astreeCreate(AST_DECL_GL, 0, $1, 0, 0, 0); }
        | decl_vetor ';' 	{ $$ = astreeCreate(AST_DECL_GL, 0, $1, 0, 0, 0); }
        ;

        decl_var : tipo_var TK_IDENTIFIER ':' ini_var 		{ $$ = astreeCreate(AST_DECL_VAR, $2, $1, $4, 0, 0); }
                ;
	decl_ponteiro : tipo_var '$'TK_IDENTIFIER ':' ini_var 	{ $$ = astreeCreate(AST_DECL_PONT, $3, $1, $5, 0, 0); }
                ;

        decl_vetor : tipo_var TK_IDENTIFIER '[' LIT_INTEGER ']' 		{ $$ = astreeCreate(AST_DECL_VEC, $2, $1, astreeCreate(AST_VEC_SIZE, $4, 0, 0, 0, 0), 0, 0); }
		| tipo_var TK_IDENTIFIER '[' LIT_INTEGER ']' ':' ini_vetor 	{ $$ = astreeCreate(AST_DECL_VEC, $2, $1, astreeCreate(AST_VEC_SIZE, $4, 0, 0, 0, 0), $7, 0); }
                ;

	ini_var :  literal 		{ $$ = $1; }
		;		
	ini_vetor :  literal ini_vetor	{ $$ = astreeCreate(AST_INI_VEC, 0, $1, $2, 0, 0); }
		|			{ $$ = 0; }
		;

tipo_var : KW_WORD	{ $$ = astreeCreate(AST_T_WOR, 0, 0, 0, 0, 0); }
        | KW_BYTE	{ $$ = astreeCreate(AST_T_BYT, 0, 0, 0, 0, 0); }
        | KW_BOOL	{ $$ = astreeCreate(AST_T_BOO, 0, 0, 0, 0, 0); }
        ;

def_funcao : tipo_var TK_IDENTIFIER '(' lista_parametros ')' var_local comando 	{ $$ = astreeCreate(AST_DEF_F, $2, $1, $4, $6, $7); }
        ;
        
        var_local: decl_var ';' var_local                       { $$ = astreeCreate(AST_DECL_LOC, 0, $1, $3, 0, 0); }
                |                                               { $$ = 0;}
                ;
        lista_parametros : lista_parametros_nao_vazia 	{ $$ = $1; }
                |					{ $$ = 0; }
                ;

        lista_parametros_nao_vazia : parametro ',' lista_parametros_nao_vazia	{ $$ = astreeCreate(AST_LIST_P, 0, $1, $3, 0, 0); }
                | parametro							{ $$ = astreeCreate(AST_LIST_P, 0, $1, 0, 0, 0); }
                ;

        parametro : tipo_var TK_IDENTIFIER	{ $$ = astreeCreate(AST_PARAM, $2, $1, 0, 0, 0); }
		| tipo_var '$'TK_IDENTIFIER	{ $$ = astreeCreate(AST_PARAM, $3, $1, 0, 0, 0); }
                ;

        comando : comando_nao_vazio     { $$ = $1; }
                |		        { $$ = 0; }
                ;
                
                comando_nao_vazio: bloco_comando		{ $$ = astreeCreate(AST_COM, 0, $1, 0, 0, 0); }
                        | controle_fluxo	                { $$ = astreeCreate(AST_COM, 0, $1, 0, 0, 0); }
                        | atribuicao		                { $$ = astreeCreate(AST_COM, 0, $1, 0, 0, 0); }
                        | input			                { $$ = astreeCreate(AST_COM, 0, $1, 0, 0, 0); }
                        | output		                { $$ = astreeCreate(AST_COM, 0, $1, 0, 0, 0); }
                        | return		                { $$ = astreeCreate(AST_COM, 0, $1, 0, 0, 0); }
                        ;

                bloco_comando : '{' seq_comando '}'	{ $$ = astreeCreate(AST_BLO_COM, 0, $2, 0, 0, 0); }
                        ;

                        seq_comando : comando_nao_vazio ';'seq_comando  	{ $$ = astreeCreate(AST_SEQ, 0, $1, $3, 0, 0); }
                                | comando		{ $$ = 0;}
                                ;

                atribuicao : TK_IDENTIFIER '=' expressao		{ $$ = astreeCreate(AST_ATR_VAR, $1, $3, 0, 0, 0); }
                        | TK_IDENTIFIER '[' expressao ']' '=' expressao	{ $$ = astreeCreate(AST_ATR_VEC, $1, $3, $6, 0, 0); }
                        ;

                input : KW_INPUT TK_IDENTIFIER			{ $$ = astreeCreate(AST_INP, $2, 0, 0, 0, 0); }
                        ;

                output : KW_OUTPUT lista_expressoes_nao_vazia	{ $$ = astreeCreate(AST_OUT, 0, $2, 0, 0, 0); }
                        ;

                        lista_expressoes_nao_vazia : expressao ',' lista_expressoes_nao_vazia	{ $$ = astreeCreate(AST_LIST_E, 0, $1, $3, 0, 0); }
                                | expressao							{ $$ = astreeCreate(AST_LIST_E, 0, $1, 0, 0, 0); }
                                ;

                return : KW_RETURN expressao	{ $$ = astreeCreate(AST_RET, 0, $2, 0, 0, 0); }
                        ;

                controle_fluxo : KW_IF '(' expressao ')' KW_THEN comando		{ $$ = astreeCreate(AST_IF, 0, $3, $6, 0, 0); }
                        | KW_IF '(' expressao ')'  KW_THEN comando KW_ELSE comando	{ $$ = astreeCreate(AST_IF_ELSE, 0, $3, $6, $8, 0); }
                        | KW_LOOP '(' comando ';' expressao ';' comando ')' comando	{ $$ = astreeCreate(AST_LOOP, 0, $3, $5, $7, $9); }
                        ;

                expressao : TK_IDENTIFIER						{ $$ = astreeCreate(AST_SYMBOL, $1, 0, 0, 0, 0); }
        		| TK_IDENTIFIER '[' expressao ']'                       	{ $$ = astreeCreate(AST_SYMBOL_VEC, $1, $3, 0, 0, 0); }
                        | literal							{ $$ = $1; }
                        | expressao '+' expressao                                      	{ $$ = astreeCreate(AST_OP_SUM, 0, $1, $3, 0, 0); }
                        | expressao '-' expressao                                       { $$ = astreeCreate(AST_OP_SUB, 0, $1, $3, 0, 0); }
                        | expressao '*' expressao                                       { $$ = astreeCreate(AST_OP_MUL, 0, $1, $3, 0, 0); }
                        | expressao '/' expressao                                       { $$ = astreeCreate(AST_OP_DIV, 0, $1, $3, 0, 0); }
                        | expressao '<' expressao                                       { $$ = astreeCreate(AST_OP_LES, 0, $1, $3, 0, 0); }
                        | expressao '>' expressao                                       { $$ = astreeCreate(AST_OP_GRE, 0, $1, $3, 0, 0); }
                        | '(' expressao ')'                                             { $$ = astreeCreate(AST_PARENTESIS, 0, $2, 0, 0, 0); }
			| expressao OPERATOR_LE expressao                    	   	{ $$ = astreeCreate(AST_OP_LE, 0, $1, $3, 0, 0); }
			| expressao OPERATOR_GE expressao                       	{ $$ = astreeCreate(AST_OP_GE, 0, $1, $3, 0, 0); }
			| expressao OPERATOR_EQ expressao                       	{ $$ = astreeCreate(AST_OP_EQ, 0, $1, $3, 0, 0); }
			| expressao OPERATOR_NE expressao                       	{ $$ = astreeCreate(AST_OP_NE, 0, $1, $3, 0, 0); }
			| expressao OPERATOR_AND expressao                      	{ $$ = astreeCreate(AST_OP_AND, 0, $1, $3, 0, 0); }
			| expressao OPERATOR_OR expressao                       	{ $$ = astreeCreate(AST_OP_OR, 0, $1, $3, 0, 0); }
                        | TK_IDENTIFIER '(' lista_expressoes ')'			{ $$ = astreeCreate(AST_CHAM_F, $1, $3, 0, 0, 0); }
			| '$'TK_IDENTIFIER						{ $$ = astreeCreate(AST_POINTER_VALUE, $2, 0, 0, 0, 0); }
			| '&'TK_IDENTIFIER						{ $$ = astreeCreate(AST_POINTER_ADDRESS, $2, 0, 0, 0, 0); }
                        ;

                        lista_expressoes : lista_expressoes_nao_vazia	{ $$ = $1; }
                                |					{ $$ = 0; }
				;
                literal : LIT_INTEGER	{ $$ = astreeCreate(AST_SYMBOL_LIT_INT, $1, 0, 0, 0, 0); }
                        | LIT_FALSE	{ $$ = astreeCreate(AST_SYMBOL_LIT_FALSE, $1, 0, 0, 0, 0); }
                        | LIT_TRUE	{ $$ = astreeCreate(AST_SYMBOL_LIT_TRUE, $1, 0, 0, 0, 0); }
                        | LIT_CHAR	{ $$ = astreeCreate(AST_SYMBOL_LIT_CHAR, $1, 0, 0, 0, 0); }
                        | LIT_STRING	{ $$ = astreeCreate(AST_SYMBOL_LIT_STRING, $1, 0, 0, 0, 0); }
			;
%%

int yyerror(char *t) {
        printf("Syntax Error on line %d\n", getLineNumber());
        exit(3);
}