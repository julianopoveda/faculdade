/*Juliano Poveda 213995
Luiz Fernando Ramos Cardozo 207113*/

#include <stdlib.h>
#include "astree.h"

ASTREE *astreeCreate(int type, HASH_NODE *symbol, ASTREE *son0, ASTREE *son1, ASTREE *son2, ASTREE *son3)
{
	ASTREE *newnode = 0;
	
	newnode = (ASTREE*)calloc(1, sizeof(ASTREE));
	newnode->type = type;
	newnode->symbol = symbol;
	newnode->son[0] = son0;
	newnode->son[1] = son1;
	newnode->son[2] = son2;
	newnode->son[3] = son3;
	
	return newnode;
}

void astreePrint(ASTREE *node, int level)
{
	int i = 0;
	if(!node)
		return;
	for (i=0; i< level; ++i)
		fprintf(stderr, "	");
	fprintf(stderr, "ASTREE(");
	if(node->symbol)
		fprintf(stderr, "[%s]\n", node->symbol->text);
	
	switch(node->type)
	{
		case AST_SYMBOL: fprintf(stderr, "AST_SYMBOL"); break;
		case AST_SYMBOL_VEC: fprintf(stderr, "AST_SYMBOL_VEC"); break;
		case AST_SYMBOL_LIT: fprintf(stderr, "AST_SYMBOL_LIT"); break;
		case AST_SYMBOL_LIT_INT: fprintf(stderr, "AST_SYMBOL_LIT_INT"); break;
		case AST_SYMBOL_LIT_TRUE: fprintf(stderr, "AST_SYMBOL_LIT_TRUE"); break;
		case AST_SYMBOL_LIT_FALSE: fprintf(stderr, "AST_SYMBOL_LIT_FALSE"); break;
		case AST_SYMBOL_LIT_CHAR: fprintf(stderr, "AST_SYMBOL_LIT_CHAR"); break;
		case AST_SYMBOL_LIT_STRING: fprintf(stderr, "AST_SYMBOL_LIT_STRING"); break;
		
		case AST_OP_SUM: fprintf(stderr, "AST_OP_SUM"); break;
		case AST_OP_SUB: fprintf(stderr, "AST_OP_SUB"); break;
		case AST_OP_MUL: fprintf(stderr, "AST_OP_MUL"); break;
		case AST_OP_DIV: fprintf(stderr, "AST_OP_DIV"); break;
		case AST_OP_LES: fprintf(stderr, "AST_OP_LES"); break;
		case AST_OP_GRE: fprintf(stderr, "AST_OP_GRE"); break;
		case AST_OP_LE: fprintf(stderr, "AST_OP_LE"); break;
		case AST_OP_GE: fprintf(stderr, "AST_OP_GE"); break;
		case AST_OP_EQ: fprintf(stderr, "AST_OP_EQ"); break;
		case AST_OP_NE: fprintf(stderr, "AST_OP_NE"); break;
		case AST_OP_AND: fprintf(stderr, "AST_OP_AND"); break;
		case AST_OP_OR: fprintf(stderr, "AST_OP_OR"); break;
		
		case AST_LIST_E: fprintf(stderr, "AST_LIST_E"); break;
        case AST_CHAM_F: fprintf(stderr, "AST_CHAM_F"); break;
        
        case AST_IF: fprintf(stderr, "AST_IF"); break; 
        case AST_LOOP: fprintf(stderr, "AST_LOOP"); break;
        
        case AST_RET: fprintf(stderr, "AST_RET"); break;
        case AST_INP: fprintf(stderr, "AST_INP"); break;
        case AST_OUT: fprintf(stderr, "AST_OUT"); break;
        
        case AST_ATR_VAR: fprintf(stderr, "AST_ATR_VAR"); break;
        case AST_ATR_VEC: fprintf(stderr, "AST_ATR_VEC"); break;
        
        case AST_SEQ: fprintf(stderr, "AST_SEQ"); break;
        case AST_PARAM: fprintf(stderr, "AST_PARAM"); break;
        
        case AST_T_INT: fprintf(stderr, "AST_T_INT"); break;
        case AST_T_WOR: fprintf(stderr, "AST_T_WOR"); break;
        case AST_T_BOO: fprintf(stderr, "AST_T_BOO"); break;
        case AST_T_CHA: fprintf(stderr, "AST_T_CHA"); break;
        
        case AST_LIST_P: fprintf(stderr, "AST_LIST_P"); break;
        
        case AST_DEF_F: fprintf(stderr, "AST_DEF_F"); break;
        case AST_DECL_VEC: fprintf(stderr, "AST_DECL_VEC"); break;
        case AST_VEC_SIZE: fprintf(stderr, "AST_VEC_SIZE"); break;
        case AST_DECL_VAR: fprintf(stderr, "AST_DECL_VAR"); break;
        case AST_DECL_GL: fprintf(stderr, "AST_DECL_GL"); break;
        
        case AST_PROG: fprintf(stderr, "AST_PROG"); break;
        
        case AST_COM: fprintf(stderr, "AST_COM"); break;
        
        case AST_BLO_COM: fprintf(stderr, "AST_BLO_COM"); break;
        
        case AST_EMPTY: fprintf(stderr, "AST_EMPTY"); break;
        
        case AST_T_BYT: fprintf(stderr, "AST_T_BYT"); break;
        case AST_INI_VEC: fprintf(stderr, "AST_INI_VEC"); break;
        case AST_POINTER_VALUE: fprintf(stderr, "AST_POINTER_VALUE"); break;
        case AST_POINTER_ADDRESS: fprintf(stderr, "AST_POINTER_ADDRESS"); break;
        
        case AST_DECL_PONT: fprintf(stderr, "AST_DECL_PONT"); break;
        case AST_PARENTESIS: fprintf(stderr, "AST_PARENTESIS"); break;
        case AST_IF_ELSE: fprintf(stderr, "AST_PARENTESIS"); break;
        
		
		default: fprintf(stderr, "UNKNOWN"); break;
	}
	
	for(i = 0; i< MAX_SONS; ++i)
	{
		fprintf(stderr, "\n");
		astreePrint(node->son[i], level+1);
	}
	
	fprintf(stderr, ");\n");
}

void astreePrintSingleCode(ASTREE *node, FILE *file) {
	
	if (!node) return;
	
	switch (node->type){
		case AST_PROG:
			astreePrintSingleCode(node->son[0], file);
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_T_WOR:
			fprintf(file, "word ");
			break;
		case AST_T_BOO:
			fprintf(file, "bool ");
			break;
		case AST_T_BYT:
			fprintf(file, "byte ");
			break;
		case AST_DECL_VAR:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file,"%s", node->symbol->text);
			fprintf(file, " : ");
			astreePrintSingleCode(node->son[1], file);
			fprintf(file, ";\n");
			break;
		case AST_DECL_VEC:
			if (node->son[2] == 0) {
				astreePrintSingleCode(node->son[0], file);
				fprintf(file,"%s", node->symbol->text);
				fprintf(file,"[");
				astreePrintSingleCode(node->son[1], file);
				fprintf(file,"]");
				fprintf(file, ";\n");
			} else {
				astreePrintSingleCode(node->son[0], file);
				fprintf(file,"%s", node->symbol->text);
				fprintf(file,"[");
				astreePrintSingleCode(node->son[1], file);
				fprintf(file,"] : ");
				astreePrintSingleCode(node->son[2], file);
				fprintf(file, ";\n");
			}
			break;
		case AST_INI_VEC:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file," ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_DECL_PONT:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, "$%s : ", node->symbol->text);
			astreePrintSingleCode(node->son[1], file);
			fprintf(file, ";\n");
			break;
		case AST_DEF_F:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, "%s(", node->symbol->text);
			astreePrintSingleCode(node->son[1], file);
			fprintf(file, ")\n");
			astreePrintSingleCode(node->son[2], file);
			astreePrintSingleCode(node->son[3], file);
			fprintf(file, "\n");
			break;
		case AST_PARAM: 
			astreePrintSingleCode(node->son[0], file);
			if (node->son[1] == 0) {
				fprintf(file, "%s", node->symbol->text);
			} else {
				fprintf(file, "%s, ", node->symbol->text);
			}
			astreePrintSingleCode(node->son[1], file);
			break;
			
			case AST_LIST_P:
				astreePrintSingleCode(node->son[0], file);
				if (node->son[1] != 0) {
					fprintf(file, "%s", ",");
					astreePrintSingleCode(node->son[1], file);
				}
			break;
			
		case AST_IF:
			fprintf(file, "if (");
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, ") then ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_IF_ELSE:
			fprintf(file, "if (");
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, ") then ");
			astreePrintSingleCode(node->son[1], file);
			fprintf(file, "\nelse\n");
			astreePrintSingleCode(node->son[2], file);
			break;
		case AST_LOOP:
			fprintf(file, "loop");
			fprintf(file, "(");
			astreePrintSingleCode(node->son[0], file);	
			fprintf(file, ";");
			astreePrintSingleCode(node->son[1], file);
			fprintf(file, ";");
			astreePrintSingleCode(node->son[2], file);			
			fprintf(file, ")\n");
			astreePrintSingleCode(node->son[3], file);
			if(node->son[3]!=0)
				fprintf(file, ";\n");
				
			break;
		case AST_OP_SUM:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " + ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_SUB:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " - ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_MUL:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " * ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_DIV:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " / ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_PARENTESIS:
			fprintf(file, "(");
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, ")");
			break;
		case AST_SYMBOL:
		case AST_SYMBOL_LIT_INT:
		case AST_SYMBOL_LIT_TRUE:
		case AST_SYMBOL_LIT_FALSE:
		case AST_SYMBOL_LIT_CHAR:
		case AST_SYMBOL_LIT_STRING:
		case AST_VEC_SIZE:
			fprintf(file, "%s", node->symbol->text);
			break;
		case AST_SYMBOL_VEC:
			fprintf(file, "%s[", node->symbol->text);
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, "]");
			break;
		case AST_OP_LE:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " <= ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_GE:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " >= ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_EQ:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " == ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_NE:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " != ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_AND:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " && ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_OR:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " || ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_GRE:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " > ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_OP_LES:
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, " < ");
			astreePrintSingleCode(node->son[1], file);
			break;
		case AST_POINTER_ADDRESS:
			fprintf(file, "&");
			fprintf(file, "%s", node->symbol->text);
			break;
		case AST_POINTER_VALUE:
			fprintf(file, "$");
			fprintf(file, "%s", node->symbol->text);
			break;
		case AST_CHAM_F:
			fprintf(file, "%s", node->symbol->text);
			fprintf(file, "(");
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, ")");
			break;
		case AST_LIST_E:
			astreePrintSingleCode(node->son[0], file);
			if(node->son[1] != 0) {
				fprintf(file, ", ");
				astreePrintSingleCode(node->son[1], file);
			}
			break;
		case AST_BLO_COM:
			fprintf(file, "{\n");
			if(node->son[0] != 0) {
				astreePrintSingleCode(node->son[0], file);
			}
			fprintf(file, "}");
			break;
        case AST_SEQ:            
			if(node->son[0] != 0) {
				astreePrintSingleCode(node->son[0], file);
            }
            if(node->son[1] != 0) {
				astreePrintSingleCode(node->son[1], file);
			}
			break;
		case AST_ATR_VAR:
			fprintf(file, "%s", node->symbol->text);
			fprintf(file, " = ");
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, ";\n");
			break;
		case AST_ATR_VEC:
			fprintf(file, "%s", node->symbol->text);
			fprintf(file, "[");
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, "] = ");
			astreePrintSingleCode(node->son[1], file);
			fprintf(file, ";\n");
			break;
		case AST_INP:
			printf("entao input\n");
			fprintf(file, "input ");
			fprintf(file, "%s", node->symbol->text);
			fprintf(file, ";\n");
			break;
		case AST_OUT:
			fprintf(file, "output ");
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, ";\n");
			break;
		case AST_RET:
			fprintf(file, "return ");
			astreePrintSingleCode(node->son[0], file);
			fprintf(file, ";\n");
			break;	
			
		case AST_DECL_GL:
			astreePrintSingleCode(node->son[0], file);
			break;
		case AST_DECL_LOC:
			astreePrintSingleCode(node->son[0], file);
			if(node->son[1] != 0) {
				astreePrintSingleCode(node->son[1], file);
			}
			break;
		case AST_COM:
			astreePrintSingleCode(node->son[0], file);
			if(node->son[0] == 0) {
				fprintf(file, "\n");
            }
			break;
		
		default:
			fprintf(file, "ERROR %d" , node->type);
			break;
	}
}

void astreePrintCode(ASTREE *node) {
	int i = 0;
	char input[256];
	FILE *file = fopen("temp.txt", "r");

    if (file == NULL) {
        fprintf(stderr, "Bad file\n");
        exit(1);
    }

    fscanf(file, "%s", input);
	fclose(file);

	if ((file = fopen(input, "w")) == NULL) {
        fprintf(stderr, "Bad file\n");
        exit(1);
    }
	
	if (!node) return;

	for (i = 0; i < MAX_SONS; ++i) {
		if (node->son[i]) astreePrintSingleCode(node->son[i], file);
	}
	fclose(file);
}