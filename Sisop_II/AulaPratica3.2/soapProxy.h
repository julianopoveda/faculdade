/* soapProxy.h
   Generated by gSOAP 2.8.16 from controleEstoque.h

Copyright(C) 2000-2013, Robert van Engelen, Genivia Inc. All Rights Reserved.
The generated code is released under one of the following licenses:
GPL or Genivia's license for commercial use.
This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
*/

#ifndef soapProxy_H
#define soapProxy_H
#include "soapH.h"

class SOAP_CMAC Proxy : public soap
{ public:
	/// Endpoint URL of service 'Proxy' (change as needed)
	const char *soap_endpoint;
	/// Constructor
	Proxy();
	/// Construct from another engine state
	Proxy(const struct soap&);
	/// Constructor with endpoint URL
	Proxy(const char *url);
	/// Constructor with engine input+output mode control
	Proxy(soap_mode iomode);
	/// Constructor with URL and input+output mode control
	Proxy(const char *url, soap_mode iomode);
	/// Constructor with engine input and output mode control
	Proxy(soap_mode imode, soap_mode omode);
	/// Destructor frees deserialized data
	virtual	~Proxy();
	/// Initializer used by constructors
	virtual	void Proxy_init(soap_mode imode, soap_mode omode);
	/// Delete all deserialized data (with soap_destroy and soap_end)
	virtual	void destroy();
	/// Delete all deserialized data and reset to default
	virtual	void reset();
	/// Disables and removes SOAP Header from message
	virtual	void soap_noheader();
	/// Get SOAP Header structure (NULL when absent)
	virtual	const SOAP_ENV__Header *soap_header();
	/// Get SOAP Fault structure (NULL when absent)
	virtual	const SOAP_ENV__Fault *soap_fault();
	/// Get SOAP Fault string (NULL when absent)
	virtual	const char *soap_fault_string();
	/// Get SOAP Fault detail as string (NULL when absent)
	virtual	const char *soap_fault_detail();
	/// Close connection (normally automatic, except for send_X ops)
	virtual	int soap_close_socket();
	/// Force close connection (can kill a thread blocked on IO)
	virtual	int soap_force_close_socket();
	/// Print fault
	virtual	void soap_print_fault(FILE*);
#ifndef WITH_LEAN
	/// Print fault to stream
#ifndef WITH_COMPAT
	virtual	void soap_stream_fault(std::ostream&);
#endif

	/// Put fault into buffer
	virtual	char *soap_sprint_fault(char *buf, size_t len);
#endif

	/// Web service operation 'adicionarEstoque' (returns error code or SOAP_OK)
	virtual	int adicionarEstoque(struct ns__Estoque *estoque, char **msgretorno) { return this->adicionarEstoque(NULL, NULL, estoque, msgretorno); }
	virtual	int adicionarEstoque(const char *endpoint, const char *soap_action, struct ns__Estoque *estoque, char **msgretorno);

	/// Web service operation 'alterarEstoque' (returns error code or SOAP_OK)
	virtual	int alterarEstoque(struct ns__Pedido *pedido, char **msgretorno) { return this->alterarEstoque(NULL, NULL, pedido, msgretorno); }
	virtual	int alterarEstoque(const char *endpoint, const char *soap_action, struct ns__Pedido *pedido, char **msgretorno);

	/// Web service one-way send operation 'send_listarEstoque' (returns error code or SOAP_OK)
	virtual	int send_listarEstoque() { return this->send_listarEstoque(NULL, NULL); }
	virtual	int send_listarEstoque(const char *endpoint, const char *soap_action);
	/// Web service one-way receive operation 'recv_listarEstoque' (returns error code or SOAP_OK);
	virtual	int recv_listarEstoque(struct ns__listarEstoque&);
	/// Web service receive of HTTP Accept acknowledgment for one-way send operation 'send_listarEstoque' (returns error code or SOAP_OK)
	virtual	int recv_listarEstoque_empty_response() { return soap_recv_empty_response(this); }
	/// Web service one-way synchronous send operation 'listarEstoque' with HTTP Accept/OK response receive (returns error code or SOAP_OK)
	virtual	int listarEstoque() { return this->listarEstoque(NULL, NULL); }
	virtual	int listarEstoque(const char *endpoint, const char *soap_action) { if (this->send_listarEstoque(endpoint, soap_action) || soap_recv_empty_response(this)) return this->error; return SOAP_OK; }

	/// Web service operation 'criarPedido' (returns error code or SOAP_OK)
	virtual	int criarPedido(struct ns__Pedido *pedido, char **msgretorno) { return this->criarPedido(NULL, NULL, pedido, msgretorno); }
	virtual	int criarPedido(const char *endpoint, const char *soap_action, struct ns__Pedido *pedido, char **msgretorno);

	/// Web service one-way send operation 'send_listarPedidos' (returns error code or SOAP_OK)
	virtual	int send_listarPedidos() { return this->send_listarPedidos(NULL, NULL); }
	virtual	int send_listarPedidos(const char *endpoint, const char *soap_action);
	/// Web service one-way receive operation 'recv_listarPedidos' (returns error code or SOAP_OK);
	virtual	int recv_listarPedidos(struct ns__listarPedidos&);
	/// Web service receive of HTTP Accept acknowledgment for one-way send operation 'send_listarPedidos' (returns error code or SOAP_OK)
	virtual	int recv_listarPedidos_empty_response() { return soap_recv_empty_response(this); }
	/// Web service one-way synchronous send operation 'listarPedidos' with HTTP Accept/OK response receive (returns error code or SOAP_OK)
	virtual	int listarPedidos() { return this->listarPedidos(NULL, NULL); }
	virtual	int listarPedidos(const char *endpoint, const char *soap_action) { if (this->send_listarPedidos(endpoint, soap_action) || soap_recv_empty_response(this)) return this->error; return SOAP_OK; }
};
#endif
