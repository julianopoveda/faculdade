#include "estoque.h"

int ns__adicionarProduto(struct ns__Estoque* estoque, char** msgretorno);
int ns__RemoverProduto(struct ns__Estoque* estoque, char** msgretorno);//Atualizar o estoque
int ns__listarEstoque(void*);

int ns__criarPedido(struct ns__Pedido* pedido, char** msgretorno);
int ns__removerPedido(struct ns__Pedido* pedido, char** msgretorno);
int ns__listarPedidos(void*);
