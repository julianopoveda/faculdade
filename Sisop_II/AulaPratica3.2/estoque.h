enum Pessoa{
  Fisica = 1,
  Juridica = 2,
};

enum StatusPedido{
  Pendente = 0,
  Aprovado = 1,
  Reprovado = -1
};

struct ns__Itens
{
  char codigoProduto[6];
  double valorProduto;
  int quantidade;
  struct* ns_Itens prox;
}

struct ns__Estoque{
  char codigoProduto[6];
  char* descricao;
  int quantidadeMaxima;
  int quantidade;
  int quantidadeMinima;
  double valorUnitario;
  struct* ns__Estoque first;//ponteiro para o primeiro item da lista
  struct* ns__Estoque prox;//lista encadeada
};

struct ns__Pedido{
  char codigoPedido[6];
  Pessoa Tipo;
  struct* ns_Itens itens;
  int desconto;
  double total;
  struct* ns__Pedido prox;//lista encadeada
  StatusPedido statusPedido;
};

struct ns__Estoque* estoquedb;
struct ns__Pedido* pedidosdb;
