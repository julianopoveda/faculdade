#include "soapService.h"
#include "ns.nsmap"

int main(int argc, char **argv)
{
  Service controleEstoque;

  if(controleEstoque->)

  if (argc < 2)
     controleEstoque.serve();       /* serve as CGI application */
  else
  {
    int port = atoi(argv[1]);
    if (!port)
    {
      fprintf(stderr, "Usage: controleEstoqueserver++ <port>\n");
      exit(0);
    }
    /* run iterative server on port until fatal error */
    if (calc.run(port))
    {
      calc.soap_stream_fault(std::cerr);
      exit(-1);
    }
  }

  return 0;

}

ns__Estoque* BuscarEstoque(char codigoProduto[6])
{
    struct ns__Estoque* db = estoquedb;
    for (db = estoquedb; db->prox !=NULL && strcmp(db->codigoProduto, codigoProduto ) !=0; db = db->prox ){}

    return db;
}

int Service::adicionarProduto(struct ns__Estoque* estoque, char** msgretorno)
{
  if(estoquedb == NULL)
  {
    estoque->prox = NULL;
    estoque->first = estoque;
    estoquedb = estoque;
  }
  else
  {
    struct ns__Estoque* produto = BuscarEstoque(estoque->codigoProduto);
    if(produto != NULL)
      produto->quantidade = estoque->quantidade;
    else
    {
    estoque->prox = NULL;
    estoque->first = estoquedb->first;
    estoquedb->prox = estoque;
    }

  }

  msgretorno = "Produto cadastrado com sucesso";

  return SOAP_OK;
}

int Service::removerProduto(struct ns__Estoque* estoque, char** msgretorno)
{
  struct ns__Estoque* db = estoquedb;
  struct ns__Estoque* dbant = estoquedb;
  for (db = estoquedb; db->prox !=NULL && strcmp(db->codigoProduto, estoque->codigoProduto ) !=0; db = db->prox )
    dbant = db;//TODO: pode dar erro aqui

  //Ponteiro agora aponta para o produto anterior ao removido
  if (db!=NULL)
    dbant->prox = db; 

  msgretorno = "Produto removido com sucesso";

  return SOAP_OK;
}


int alterarEstoque(struct ns_Estoque* estoque, char** msgretorno)
{
  struct ns__Itens* itens = pedido->itens;
  struct ns__Estoque* db = estoquedb;
  for(itens = pedido->itens; itens->prox !=NULL; itens = itens->prox)
  {
      //Até encontrar o produto
      for (db = estoquedb; db->prox !=NULL && strcmp(db->codigoProduto, itens->codigoProduto ) !=0; db = db->prox ){}

      if(db == NULL)
      {
        return Service::adicionarEstoque(estoque, msgretorno);
      }

      db->quantidade = estoque->quantidade;
      //TODO: o que fazer
      if(db->quantidade < db->quantidadeMaxima)
        "Repor Estoque";
  }

}

int Service::listarEstoque(void*)
{
  struct ns__Estoque* estoqueprint = estoquedb;
  for(estoqueprint = estoquedb; estoqueprint->prox != NULL; estoqueprint = estoqueprint->prox)
    printf("Codigo: %s\nDescricao: %s\nQuantidade Minima: %d\nQuantidade Maxima: %d\nValor unitario: %d\n------------\n", estoqueprint->codigoProduto, estoqueprint->descricao, estoqueprint->quantidadeMinima, estoqueprint->quantidadeMaxima, estoqueprint->valorUnitario );  

  return SOAP_OK;
}

int Service::criarPedido(struct ns__Pedido* pedido, char** msgretorno)
{
  pedido->statusPedido = StatusPedido.Pendente;

  if(pedido->Tipo == Pessoa.Fisica && pedido->desconto > 8)
  {
    pedido->statusPedido = StatusPedido.Reprovado;
    msgretorno = "Desconto acima do permitido. Pedido Reprovado";
  }
  else if(pedido->Tipo == Pessoa.Juridica && pedido->desconto > 12)
  {
    pedido->statusPedido = StatusPedido.Reprovado;
    msgretorno = "Desconto acima do permitido. Pedido Reprovado";
  }
  
 for(itens = pedido->itens; itens->prox !=NULL; itens = itens->prox)
  {
      //Até encontrar o produto
      db = BuscarEstoque(itens->codigoProduto);

      if(db == NULL)
      {
        msgretorno = "Produto" + itens->codigoProduto + " não localizado"; 
        pedido->statusPedido = StatusPedido.Reprovado;
        return -255;
      }

      if(db->quantidade < itens->quantidade)
      {
        msgretorno = "Quantidade insuficiente para atender o pedido";
        pedido->statusPedido = StatusPedido.Reprovado;
        return -254;
      }
  }
  

}

int Service::listarPedidos(void*)
{
  struct ns__Pedido* pedido = pedidosdb;
  struct ns__Itens* item;
  for(pedido = pedidosdb; pedido->prox != NULL; pedido = pedido->prox)
  {
    printf("Pedido: %s\nTipo: %d\nDesconto: %d\nTotal: %d\n\n------------\n", pedido->codigoPedido, pedido->Tipo, pedido->desconto, pedido->total);
    printf("Itens do Pedido\n");
    for(item = pedidoprint->itens; item->prox!=NULL; item=item->prox)
      printf("\tProduto: %s\nValor: %d\n", item->codigoProduto, item->valorProduto);
  }
  return SOAP_OK;
}
